$(function(){
    $('.flagSelect').removeClass('browser-default');
    $('.flagSelect').material_select();

    $('.flagIt').click(function(){
        var flagType  = $('.flagIt').attr('flag-type');

        if(flagType == 'reputation'){
            $('#repForm').openModal();
        }else if(flagType == 'mailing'){
            $('input[name="foreign_id"]').val($('.flagIt').attr('mailing-id'));
            $('#mailForm').openModal();
        }
    });

    var validateForm = function(form, cb){
        $(form).on('submit', function(e){
            e.preventDefault();
            var valid = true;
            $(this).find('input[data-required], textarea[data-required], select[data-required]').each(function(index, input){
                if($(input).val() == '' || $(input).val() == null){
                    valid = false;
                    Materialize.toast('The ' + $(input).attr('name') +' field is required', 1500);
                }
            })
            if(valid) cb();
        })
    }

    validateForm('#flagForm', function(){
        var formdata = $('#flagForm').serializeArray();
        var CSRF = $('input[name="_token"]').val();


        $.ajax({
            url: "/flagged",
            type: "POST",
            dataType: "json",
            data: formdata,
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(data){
            if(data == 'saved'){
                Materialize.toast('Your note was added', 1500,'',function(){
                    window.location.href = "/flagged";
                });
            }
        }).error(function(data){
            console.log('ERROR', data);
        });
    });

    $('.loadMore').click(function(){

        var offset = $('.loadMore').attr('data-load');

        $.ajax({
            url: "/flagged/load/" + offset,
        }).done(function(data){
            if(data){
                $('.flagsWrapper').append(data);
                $('.loadedFlags').slideDown(1000);
                $('.loadMore').attr('data-load', (parseInt(offset) + 5));
            }else{
                $('.loadMore').addClass('disabled');
                $('.loadIcon').html('clear');
            }
        }).error(function(data){
            console.log('ERROR', data);
        });
    });


});