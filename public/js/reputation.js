google.load('visualization', '1.1', {packages: ['corechart', 'line']});

$(function(){
    google.setOnLoadCallback(getLineChartData);

    $('.inputDaysRight').submit(function(e){
        e.preventDefault();
        getLineChartData();
    });

    function getLineChartData(){
        var days = $('.changeDaysInput').val();
        var CSRF = $('input[name="_token"]').val();

        $('#linechart_material').children().slideUp();
        $('#linechart_material').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/reputation/" + days,
            dataType: "json",
        }).done(function(chartData) {
           drawLineChart(chartData);
        }).error(function(data) {
            console.log('ERROR', data);
        });
    };

    function drawLineChart(chartData){
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Date');
        chartData.servers.forEach(function(server){
            data.addColumn('number', server.name);
        });

        data.addRows(
            chartData.scores
        );

        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
            columns.push(i);
            if (i > 0) {
                series[i - 1] = {};
            }
        }

        var options = {
            chartArea: {
                width: '65%'
            },
            hAxis: {
                title: 'Date'
            },
            vAxis: {
                title: 'Reputation'
            },
            width: '100%',
            height: 400,
            series: series
        }

        var chart = new google.visualization.LineChart(document.getElementById('linechart_material'));
        $('#linechart_material').children().fadeOut(100);
        setTimeout(function(){
            chart.draw(data, options);
        }, 150);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        function selectHandler(){
            if(chart.getSelection().length){
                if (chart.getSelection()[0].row != null) {
                    getChartDate();
                }else{
                    toggleLegend();
                }
            }
        }

        function getChartDate(){
            var clickedRow = chart.getSelection()[0].row;
            var clickedDate = chartData.scores[clickedRow][0];

            $.ajax({
                url: "/reputation/date/" + clickedDate,
                dataType: "json",
            }).done(function(chartData) {
                makeReputationTables(chartData);
            }).error(function(data) {
                console.log('ERROR', data);
            });
        }

        function toggleLegend () {
            var sel = chart.getSelection();
            // if selection length is 0, we deselected an element
            if (sel.length > 0) {
                // if row is undefined, we clicked on the legend
                if (sel[0].row === null) {
                    var col = sel[0].column;
                    if (columns[col] == col) {
                        // hide the data series
                        columns[col] = {
                            label: data.getColumnLabel(col),
                            type: data.getColumnType(col),
                            calc: function () {
                                return null;
                            }
                        };

                        // grey out the legend entry
                        series[col - 1].color = '#CCCCCC';
                    }
                    else {
                        // show the data series
                        columns[col] = col;
                        series[col - 1].color = null;
                    }
                    var view = new google.visualization.DataView(data);
                    view.setColumns(columns);
                    chart.draw(view, options);
                }
            }
        };
    }

    function makeReputationTables(chartData){

        $('.senderscoreBody').fadeOut('1000',function(){ $(this).remove();});
        $('.senderbaseBody').fadeOut('1000',function(){ $(this).remove();});
        $('.sndsBody').fadeOut('1000',function(){ $(this).remove();});

        if(chartData.senderscore[0].scores.length){
            var color = '';
            var senderscoreTable = '<tbody class="senderscoreBody">';
            for(var i = 0; i < chartData.senderscore.length; i++){
                if(chartData.senderscore[i].scores.length) {
                    senderscoreTable += '<tr>';
                    senderscoreTable += '<td>' + chartData.senderscore[i].ip + '</td>';
                    senderscoreTable += '<td>' + chartData.senderscore[i].name + '</td>';
                    senderscoreTable += '<td>' + chartData.senderscore[i].location + '</td>';
                    senderscoreTable += '<td>' + chartData.senderscore[i].type + '</td>';
                    if (chartData.senderscore[i].scores[0].score > 89) {
                        color = 'textGreen'
                    } else {
                        color = 'textRed'
                    }
                    senderscoreTable += '<td class="' + color + '">' + chartData.senderscore[i].scores[0].score + '</td>';
                    senderscoreTable += '<td>' + chartData.senderscore[i].scores[0].ts.substr(0, 10) + '</td>';
                    senderscoreTable += '</tr>';
                }
            }
            senderscoreTable += '</tbody>';
            $('.senderscoreTable').append(senderscoreTable);
        }else{
            $('.senderscoreTable').append('<p class="senderscoreBody">No data was found for this date.</p>')
        }

        if(chartData.senderbase[0].bases.length){
            var senderbaseTable = '<tbody class="senderbaseBody">';
            for(var i = 0; i < chartData.senderbase.length; i++){
                if(chartData.senderbase[i].bases.length) {
                    senderbaseTable += '<tr>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].ip + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].name + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].reverse_dns + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].email_reputation + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].magnitude_last_day + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].magnitude_last_month + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].volume_change + '</td>';
                    senderbaseTable += '<td>' + chartData.senderbase[i].bases[0].ts.substr(0, 10) + '</td>';
                    senderbaseTable += '</tr>';
                }
            }
            senderbaseTable += '</tbody>';
            $('.senderbaseTable').append(senderbaseTable);
        }else{
            $('.senderbaseTable').append('<p class="senderbaseBody">No data was found for this date.</p>')
        }

        if(chartData.snds[0].snds.length){
            var sndsTable = '<tbody class="sndsBody">';
            for(var i = 0; i < chartData.snds.length; i++){
                if(chartData.snds[i].snds.length) {
                    sndsTable += '<tr>';
                    sndsTable += '<td>' + chartData.snds[i].ip + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].name + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].from.substr(0, 10) + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].till.substr(0, 10) + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].rcpt + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].data + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].msg_rcpt + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].filter + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].trap + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].complaint_rate + '</td>';
                    sndsTable += '<td>' + chartData.snds[i].snds[0].helo + '</td>';
                    sndsTable += '</tr>';
                }
            }
            sndsTable += '</tbody>';
            $('.sndsTable').append(sndsTable);
        }else{
            $('.sndsTable').append('<p class="sndsBody">No data was found for this date.</p>')
        }

    }
});