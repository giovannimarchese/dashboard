google.load('visualization', '1.1', {packages: ['corechart', 'line']});

$(function(){
    var date = $('input[name="date"]').val();

    $('#overview_datatable').DataTable({
        ajax: "/domains/table/" + date,
        columns: [
            {data: "domain"},
            {data: "sent"},
            {data: "deferred"},
            {data: "percentage"}
        ],
        "fnCreatedRow": function(row, data){
            $(row)
                .addClass('domainStats')
                .attr('data-domain-name', data.domain);
        },
        "order": [[ 1, "desc" ]],
        "language": {
            "lengthMenu": "Show _MENU_",
            "loadingRecords": "<div class='progress'><div class='indeterminate'></div></div>"
        }
        //"sDom": '<"top"fl>rt<"bottom"ip>'
    });

    $('select').addClass('browser-default');

    $('.changeDateBtn').click(function(){
        var date = $('.changeDateInput').val();
        window.location.href = '/domains/' + date;
    });


    $('tbody').on("click", ".domainStats",function(){
        var domain = $(this).attr('data-domain-name');

        $('#pieHeading').empty();

        getPieChartData(domain);
    });

    function getPieChartData(name){
        var date = $('.changeDateInput').val();
        var name = name;
        var CSRF = $('input[name="_token"]').val();

        $('#piechart1').children().slideUp();
        $('#piechart2').children().slideUp();
        $('#pieHeading').append('Charts of:  ' + name);
        $('#piechart1').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/domains/pie",
            type: "POST",
            dataType: "json",
            data: {date: date, name: name},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            drawPieChart(chartData);
        }).error(function(data) {
            console.log('ERROR', data);
        });
    }

    function drawPieChart(chartData){
        var pie1 = google.visualization.arrayToDataTable([
            ['Case', 'Amount of mails'],
            ['Sent', chartData.mixed.sent],
            ['Deferred', chartData.mixed.deferred]
        ]);

        var pie2 = [];
        pie2.push(['Case', 'Deferred per server']);
        Object.keys(chartData.deferred).forEach(function (k){
            if(chartData.deferred[k].deferred != 0){
                newArr = [k, chartData.deferred[k].deferred];
                pie2.push(newArr);
            }
        });
        var pie2 = google.visualization.arrayToDataTable(pie2);

        var options1 = {
            title: 'Sent/Deferred',
            pieHole: 0.4,
            width: '100%',
            height: 400,
            sliceVisibilityThreshold: 0,

        };

        var options2 = {
            title: 'Deferred per server',
            pieHole: 0.4,
            width: '100%',
            height: 400,
            sliceVisibilityThreshold: 0,

        };

        var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
        var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
        setTimeout(function(){
            $('#piechart1').children().fadeOut(100);
            setTimeout(function(){
                chart1.draw(pie1, options1);
                chart2.draw(pie2, options2);
            }, 150);
        }, 300);
    }

    $('.datepicker').pickadate({
        min: new Date(2015,5,29),
        max: new Date(),
        selectYears: true,
        selectMonths: true,
        format: 'yyyy-mm-dd',
    });
});