google.load('visualization', '1.1', {packages: ['corechart', 'line']});

$(function(){

    $('.changeDateBtn').click(function(){
        var date1 = $('.changeDate1Input').val();
        var date2 = $('.changeDate2Input').val();
        if(date1 == '' || date2 == ''){
            Materialize.toast('You haven\'t picked a date yet', 2500);
        }else{
            getLineChartData();
        }
    });

    function getLineChartData(){
        var date1 = $('.changeDate1Input').val();
        var date2 = $('.changeDate2Input').val();
        var CSRF = $('input[name="_token"]').val();

        $('#linechart').children().slideUp();
        $('#linechart').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/volume/dates",
            type: "POST",
            dataType: "json",
            data: {date1: date1, date2: date2},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            drawLineChart(chartData);
        }).error(function(data) {
            $('#linechart').children().fadeOut(100);
            $('#linechart').append('<p>Something went wrong</p>')
            console.log('ERROR', data);
        });
    };

    function drawLineChart(chartData){
        var data = new google.visualization.DataTable();

        var formatChartData = [];
        Object.keys(chartData.total).forEach(function(type){
            var newArr = [];
            newArr.push(type);

            Object.keys(chartData).forEach(function (k){
                newArr.push(chartData[k][type]);
            });
            formatChartData.push(newArr);
        });

        data.addColumn('string', 'Time');
        Object.keys(chartData).forEach(function(type){
            data.addColumn('number', type);
        });

        data.addRows(
            formatChartData
        );

        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
            columns.push(i);
            if (i > 0) {
                series[i - 1] = {};
            }
        }

        var options = {
            chartArea: {
                width: '65%'
            },
            hAxis: {
                title: 'Dates'
            },
            vAxis: {
                title: 'Mails'
            },
            width: '100%',
            height: 400,
            series: series
        }

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        $('#linechart').children().fadeOut(100);
        setTimeout(function(){
            chart.draw(data, options);
        }, 150);


        google.visualization.events.addListener(chart, 'select', selectHandler);

        function selectHandler(){
            if(chart.getSelection().length){
                if (chart.getSelection()[0].row != null) {
                    getChartDate();
                }else{
                    toggleLegend();
                }
            }
        }

        function getChartDate(){
            var clickedRow = chart.getSelection()[0].row;
            var clickedDate = Object.keys(chartData.total)[clickedRow];

            swal({
                title: "See mailings for " + clickedDate + "?",
                text: "You will leave this page.",
                type: "warning",
                showCancelButton: true,
                closeOnCancel: true,
                confirmButtonColor: "#4DD0E1",
                confirmButtonText: "Yes, show me!",

            }, function(isConfirm){
                if(isConfirm){
                    window.location.href = "/mailings/d/" + clickedDate;
                }
            });

        }

        function toggleLegend () {
            var sel = chart.getSelection();
            // if selection length is 0, we deselected an element
            if (sel.length > 0) {
                // if row is undefined, we clicked on the legend
                if (sel[0].row === null) {
                    var col = sel[0].column;
                    if (columns[col] == col) {
                        // hide the data series
                        columns[col] = {
                            label: data.getColumnLabel(col),
                            type: data.getColumnType(col),
                            calc: function () {
                                return null;
                            }
                        };

                        // grey out the legend entry
                        series[col - 1].color = '#CCCCCC';
                    }
                    else {
                        // show the data series
                        columns[col] = col;
                        series[col - 1].color = null;
                    }
                    var view = new google.visualization.DataView(data);
                    view.setColumns(columns);
                    chart.draw(view, options);
                }
            }
        };
    }

    $('.datepicker').pickadate({
        max: new Date(),
        selectYears: true,
        selectMonths: true,
        format: 'yyyy-mm-dd',
    });

    getLineChartData();
});