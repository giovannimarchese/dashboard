$(function(){
    //init slide nav
    $(".button-collapse").sideNav();

    //make button in nav active when on page
    $('nav a[href^="/' + location.pathname.split("/")[1] + '"]').parent().addClass('active');

    //set pagetitle in html title tag
    var page = location.pathname.split("/")[1];
    $('title').text('Dashboard - ' +  page.charAt(0).toUpperCase() + page.substr(1));

    //add class to body for image on login page
    if(page == 'auth'){ $('body').addClass('login'); }

    //make into materialize tooltip
    $('.tooltip').tooltip();

    $(".dropdown-button").dropdown();
});