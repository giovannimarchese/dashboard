$(function(){

    $('.notificationSelect').material_select();

    $('.profile_img').click(function(){
        $('#profile_input').trigger('click');
    });

    $('#openNotifications').click(function() {
        $('#changeNotifications').openModal();
    });


    $('#notificationsForm').submit(function(e){
        e.preventDefault();

        var formdata = $('#notificationsForm').serializeArray();
        var CSRF = $('input[name="_token"]').val();
        
        $.ajax({
            url: "/notification",
            type: "POST",
            dataType: "json",
            data: formdata,
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(data){
            if(data == 'saved'){
                $('#changeNotifications').closeModal();
                Materialize.toast('Your notification settings are updated', 1500);
            }else{
                Materialize.toast('Something went wrong', 1500);
            }
        }).error(function(data){
            console.log('ERROR', data);
        });
    });


});

