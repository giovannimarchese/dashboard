google.load('visualization', '1.1', {packages: ['corechart', 'line']});

$(function(){
    google.setOnLoadCallback(getLineChartData);
    google.setOnLoadCallback(getPieChartData);

    setDateHtml();

    $('.changeDateBtn').click(function(){
        var date = $('.changeDateInput').val();
        if(date == ''){
            Materialize.toast('You haven\'t picked a date yet', 2500);
            console.log(date);
        }else{
            setDateHtml();
            
            getLineChartData();
            getPieChartData();
        }
    });

    $('.gotoMailingsBtn').click(function(){
        var date = $('.changeDateInput').val();
        window.location.href = '/mailings/d/' + date;
    });

    function setDateHtml(){
        $('.selectedDate').html($('.changeDateInput').val());
    }

    function getLineChartData(){
        var date = $('.changeDateInput').val();
        var CSRF = $('input[name="_token"]').val();

        $('#linechart').children().slideUp();
        $('#linechart').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/traffic/line",
            type: "POST",
            dataType: "json",
            data: {date: date},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            drawLineChart(chartData);
        }).error(function(data) {
            console.log('ERROR', data);
        });
    };

    function getPieChartData(){
        var date = $('.changeDateInput').val();
        var CSRF = $('input[name="_token"]').val();

        $('#piechart1').children().slideUp();
        $('#piechart2').children().slideUp();
        $('#piechart1').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/traffic/pie",
            type: "POST",
            dataType: "json",
            data: {date: date},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            console.log(chartData);
            makeTable(chartData);
            drawPieChart(chartData);
        }).error(function(data) {
            console.log('ERROR', data);
        });
    }

    function drawLineChart(chartData){
        var data = new google.visualization.DataTable();

        var formatChartData = [];
        Object.keys(chartData.total).forEach(function(type){
            var newArr = [];
            newArr.push(type);

            Object.keys(chartData).forEach(function (k){
                newArr.push(chartData[k][type]);
            });
            formatChartData.push(newArr);
        });

        data.addColumn('string', 'Time');
        Object.keys(chartData).forEach(function(type){
            data.addColumn('number', type);
        });

        data.addRows(
            formatChartData
        );

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        setTimeout(function(){
            $('#linechart').children().fadeOut(100);
            setTimeout(function(){
                chart.draw(data, options);
            }, 100);
        }, 250);

        var columns = [];
        var series = {};
        for (var i = 0; i < data.getNumberOfColumns(); i++) {
            columns.push(i);
            if (i > 0) {
                series[i - 1] = {};
            }
        }

        var options = {
            chartArea: {
                width: '65%'
            },
            hAxis: {
                title: 'Time'
            },
            vAxis: {
                title: 'Mails'
            },
            width: '100%',
            height: 400,
            series: series
        }

        google.visualization.events.addListener(chart, 'select', function () {
            var sel = chart.getSelection();
            // if selection length is 0, we deselected an element
            if (sel.length > 0) {
                // if row is undefined, we clicked on the legend
                if (sel[0].row === null) {
                    var col = sel[0].column;
                    if (columns[col] == col) {
                        // hide the data series
                        columns[col] = {
                            label: data.getColumnLabel(col),
                            type: data.getColumnType(col),
                            calc: function () {
                                return null;
                            }
                        };

                        // grey out the legend entry
                        series[col - 1].color = '#CCCCCC';
                    }
                    else {
                        // show the data series
                        columns[col] = col;
                        series[col - 1].color = null;
                    }
                    var view = new google.visualization.DataView(data);
                    view.setColumns(columns);
                    chart.draw(view, options);
                }
            }
        });
    }

    function drawPieChart(chartData){
        var pie1 = google.visualization.arrayToDataTable([
            ['Case', 'Deliverability of send mail today'],
            ['Delivered', chartData.delivered.delivered],
            ['Deferred', chartData.delivered.deferred],
            ['Bounced', chartData.delivered.bounced],
            ['Rejected', chartData.delivered.rejected]
        ]);

        var pie2 = [];
        pie2.push(['Case', 'Traffic per server']);
        Object.keys(chartData.server).forEach(function (k){
            newArr = [k, chartData.server[k]];
            pie2.push(newArr);
        });
        var pie2 = google.visualization.arrayToDataTable(pie2);


        var options1 = {
            title: 'Deliverability',
            pieHole: 0.4,
            width: '100%',
            height: 400,
            sliceVisibilityThreshold: 0,

        };

        var options2 = {
            title: 'Traffic per server',
            pieHole: 0.4,
            width: '100%',
            height: 400,
            sliceVisibilityThreshold: 0,

        };

        var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
        var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
        setTimeout(function(){
            $('#piechart1').children().fadeOut(100);
            setTimeout(function(){
                chart1.draw(pie1, options1);
                chart2.draw(pie2, options2);
            }, 100);
        }, 300);

    }

    function makeTable(chartData){
        var delTable = $('#deliverability');
        var volTable = $('#traffic');

        $("#deliverability>li:not(:first-child)").slideUp(200);
        $("#traffic>li:not(:first-child)").slideUp(200);

        setTimeout(function(){
            Object.keys(chartData.delivered).forEach(function (k){
                delTable.append('<li class="collection-item">'+ k.charAt(0).toUpperCase() + k.substr(1) +': '+ chartData.delivered[k]+'</li>');
            });

            Object.keys(chartData.server).forEach(function (l){
                volTable.append('<li class="collection-item">'+ l.substr(0, l.indexOf('.')) +': '+ chartData.server[l]+'</li>');
            });
        }, 200);


    }

    $('.datepicker').pickadate({
        min: new Date(2015,5,29),
        max: new Date(),
        selectYears: true,
        selectMonths: true,
        format: 'yyyy-mm-dd',
    });
});