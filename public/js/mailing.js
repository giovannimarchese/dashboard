google.load('visualization', '1', {packages: ['corechart', 'bar']});

$(function(){

    if(location.pathname.split("/")[2] != undefined){
        var urlSegment = location.pathname.split("/")[2];
        if(urlSegment.substr(0, 2) == 'MC'){
            var mailName = $('input[name="mailing"]').val();
            var oldId = $('input[name="old-id"]').val();
            var dbId = $('input[name="db-id"]').val();
            var dbName = $('input[name="db-name"]').val();
            getMailData(urlSegment.substr(2), oldId, mailName, dbId, dbName)
        }
    }

    var page = $('input[name="page"]').val();
    var url;

    if (page === 'index') {
        var month = $('input[name="month"]').val();

        url = "/mailings/table";
        if (month) url += "/" + month;
    } else if(page === 'customer') {
        var customer = $('input[name="customer"]').val();
        var date = $('input[name="date"]').val();

        url = "/mailings/c/table/" + customer;
        if (date) url += "/" + date;
    } else if(page === 'date'){
        var date = $('input[name="date"]').val();

        url = "/mailings/d/table/" + date;
    }

    $('#overview_datatable').DataTable({
        ajax: url,
        columns: [
            {data: "old_mail_id"},
            {data: "customer_db", render: function(value){return value.substr(0,20)}},
            {data: "name", render: function(value){return value.substr(0,20)}},
            {data: "mails"},
            {data: "reads"},
            {data: "clicks"},
            {data: "conversions"},
            {data: "hb"},
            {data: "sb"},
            {data: "spam"},
            {data: "timestamp"}
        ],
        "fnCreatedRow": function(row, data){
            $(row)
                .addClass('mailing')
                .attr('data-mailing', data.id)
                .attr('data-old-mailing', data.old_mail_id)
                .attr('data-mail-name', data.name)
                .attr('data-db-id', data.db_id)
                .attr('data-db-name', data.customer_db);

            $('td', row).eq(1)
                .addClass('tooltip')
                .attr('data-position', 'right')
                .attr('data-delay', 50)
                .attr('data-tooltip', data.customer_db);

            $('td', row).eq(2)
                .addClass('tooltip')
                .attr('data-position', 'right')
                .attr('data-delay', 50)
                .attr('data-tooltip', data.name);
        },
        "fnDrawCallback": function() {
            $('#overview_datatable .tooltip').tooltip();
        },
        "order": [[ 10, "desc" ]],
        "language": {
            "lengthMenu": "Show _MENU_",
            "loadingRecords": "<div class='progress'><div class='indeterminate'></div></div>"
        }
    });


    $('select').addClass('browser-default');

    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: true,
        format: 'yyyy-mm-dd',
    });

    $('#customerSelect').select2();

    $('#customerSelect').change(function(){
        var CSRF = $('input[name="_token"]').val();
        var customer_id = $('#customerSelect').val();

        if(customer_id != ""){
            $('.databases').removeClass('hide');
            $.ajax({
                url: "/mailings",
                type: "POST",
                dataType: "json",
                data: {id: customer_id},
                headers: {'X-CSRF-TOKEN': CSRF}
            }).done(function(data) {
                fillSelectbox(data);
            }).error(function(data) {
                console.log('ERROR', data);
            });
        }else{
            $('.databases').addClass('hide');
        }
    });

    function fillSelectbox(data){
        $('#databaseSelect').empty();
        $('#databaseSelect').append('<option value="">Database</option>');
        for(var i = 0; i < data.length; i++){
            $('#databaseSelect').append('<option value="'+ data[i].id +'">'+ data[i].name +'</option>');
        }
        $('#databaseSelect').select2();
    }

    $('#loadMails').click(function(){
        var month = $('#monthSelect').val();
        window.location.href = '/mailings/' + month;
    });

    $('#getMails').click(function(){
        var customer = $('#customerSelect').val();
        var database = $('#databaseSelect').val();
        var date = $('.datepicker').val();

        if(customer == ''){
            if(date == ''){
                Materialize.toast('You need to pick a customer/database and/or a date', 2500);
            }else{
                window.location.href = '/mailings/d/' + date;
            }
        }else if(database == ''){
            if(date == ''){
                window.location.href = '/mailings/c/C' + customer;
            }else{
                window.location.href = '/mailings/c/C' + customer + '/' + date;
            }
        }else if(database != ''){
            if(date == ''){
                window.location.href = '/mailings/c/D' + database;
            }else{
                window.location.href = '/mailings/c/D' + database + '/' + date;
            }
        }
    });



    $('tbody').on("click", ".mailing",function(){
        var id = $(this).attr('data-mailing');
        var mailId = $(this).attr('data-old-mailing');
        var mailName = $(this).attr('data-mail-name');
        var dbId = $(this).attr('data-db-id');
        var dbName = $(this).attr('data-db-name');

        getMailData(id, mailId, mailName, dbId, dbName);
    });

    function getMailData(id, mailId, mailName, dbId, dbName){
        var CSRF = $('input[name="_token"]').val();

        $('.smallBtn').addClass('hide');
        $('.dbBtn').attr('db-id', dbId).attr('mailing-id', mailId);
        $('.flagIt').attr('mailing-id', id);
        $('.mailname').empty();
        $('.customername').empty();
        $('#barchart').children().slideUp();
        $('#barchart').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/mailings/bar",
            type: "POST",
            dataType: "json",
            data: {id: id},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            if(chartData == 'notfound'){
                $('#barchart').children().fadeOut(100);
                $('#barchart').append('<p>We couldn\'t find any data for this mailing.</p>');
            }else if(chartData.total == 0){
                $('#barchart').children().fadeOut(100);
                $('#barchart').append('<p>The total amount of mails was 0, you don\'t need a chart for that.</p>');
            }else{
                drawBarChart(chartData, mailName, dbName);
            }
        }).error(function(data) {
            console.log('ERROR', data);
        });
    };

    function drawBarChart(chartData, mailName, dbName){


        var data = google.visualization.arrayToDataTable([
            ['Deliverability', 'Num. of mailings'],
            ['Total', chartData.total ],
            ['Reads', chartData.reads ],
            ['Clicks', chartData.clicks  ],
            ['Conver.', chartData.conversions  ],
            ['Hardb.', chartData.hb ],
            ['Softb.', chartData.sb ],
            ['Spam', chartData.spam ]
        ]);

        var options = {
            width: '100%',
            legend: {
                position: 'none'
            },
            chart: {
                title: 'Deliverability data of ' + mailName
            },
            hAxis: {
                title: 'Number of mailings',
                logScale: true,
            },
            vAxis: {
                title: 'Deliverability'
            },
            bars: 'horizontal'
        };

        var chart = new google.visualization.BarChart(document.getElementById('barchart'));
        setTimeout(function(){
            $('#barchart').children().fadeOut(100);
            $('.mailname').append(mailName);
            $('.customername').append(dbName);
            $('.smallBtn').removeClass('hide');
            setTimeout(function(){
                chart.draw(data, options);
            }, 150);
        }, 300);
    }

    $('.spam').click(function(){
        var dbid  = $(this).attr('db-id');
        var mail = $(this).attr('mailing-id');

        $('.complaints').empty();
        $('.spamcount').empty();
        $('#pieChart').empty();

        $('#complaintModal').append('<div class="progress"><div class="indeterminate"></div></div>');
        $('#complaintModal').openModal();
        $.ajax({
            url: "/mailings/spam/" + mail + "/" + dbid,
            dataType: "json",
        }).done(function(data) {
            $('.progress').remove();
            for(var i = 0; i < data.complainers.length; i++){
                $('.complaints').append('<p>'+data.complainers[i].subscriber+'</p>');
            }
            $('.spamcount').append(data.complainers.length);

            makePieChart(data.chart);

        }).error(function(data) {
            console.log('ERROR', data);
        });
    });

    $('.hardbounce').click(function(){
        var dbid  = $(this).attr('db-id');
        var mail = $(this).attr('mailing-id');

        $('.hardbounces').empty();
        $('.hbcount').empty();

        $('.nohb').addClass('hide');
        $('.hbtable').addClass('hide');

        $('#hardbounceModal').append('<div class="progress"><div class="indeterminate"></div></div>');
        $('#hardbounceModal').openModal();

        $.ajax({
            url: "/mailings/hb/" + mail + "/" + dbid,
            dataType: "json",
        }).done(function(data) {
            $('.progress').remove();
            if(data == 0){
                $('.nohb').removeClass('hide');
            }else{
                var count = 0;
                for(var i = 0; i < data.length; i++){
                    count += data[i].amount;
                    $('.hardbounces').append('<tr><td>' + data[i].domain + '</td><td>' + data[i].code + '</td><td>' + data[i].status + '</td><td>' + data[i].amount + '</td></tr>');
                }
                $('.hbtable').removeClass('hide');
                if(count > 1){
                    $('.hbcount').append(count);
                }
            }
        }).error(function(data) {
            console.log('ERROR', data);
        });
    });

    function makePieChart(chart){

        var pie = [];
        pie.push(['Domain', 'Mails']);
        Object.keys(chart).forEach(function (k){
            newArr = [k, chart[k]];
            pie.push(newArr);
        });
        var pie = google.visualization.arrayToDataTable(pie);

        var options = {
            pieHole: 0.4,
            width: '100%',
            height: 350,
            sliceVisibilityThreshold: 0,
            legend: 'none',
            backgroundColor: '#fafafa',
            chartArea: {'width': '95%'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('pieChart'));
        setTimeout(function(){
            chart.draw(pie, options);
        }, 100);

    }


});