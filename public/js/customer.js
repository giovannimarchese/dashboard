google.load('visualization', '1.1', {packages: ['corechart', 'line']});

$(function(){

    $('.customer').click(function(){
        var id = $(this).attr('data-customer');
        var cusName = $(this).attr('data-cus-name');
        var defaultMonthAmount = 12;

        $('.changeMonthsInput').val(defaultMonthAmount);

        getCustomerData(id, cusName, defaultMonthAmount);
    });

    $('.changeMonthsBtn').click(function(){
        var id = $('.changeMonthsInput').attr('data-customer');
        var cusName = $('.changeMonthsInput').attr('data-cus-name');
        var months = $('.changeMonthsInput').val();
        console.log(id);
        getCustomerData(id, cusName, months);
    });

    function getCustomerData(id, cusName, months){
        var CSRF = $('input[name="_token"]').val();

        $('#linechart').children().slideUp();
        $('.chartheading').empty();
        $('.inputMonthsRight').addClass('hide');
        $('#linechart').append('<div class="progress"><div class="indeterminate"></div></div>');

        $.ajax({
            url: "/customers",
            type: "POST",
            dataType: "json",
            data: {id: id, months: months},
            headers: {'X-CSRF-TOKEN': CSRF}
        }).done(function(chartData) {
            drawLineChart(chartData, cusName);
            $('.changeMonthsInput').attr('data-customer', id);
            $('.changeMonthsInput').attr('data-cus-name', cusName);
        }).error(function(data) {
            console.log('ERROR', data);
        });
    };

    function drawLineChart(chartData, cusName){
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Month');
        data.addColumn('number', 'Mailings');

        data.addRows(
            chartData
        );

        var options = {
            legend: {
                position: 'none'
            },
            chartArea: {
                width: '85%'
            },
            hAxis: {
                title: 'Months'
            },
            vAxis: {
                title: 'Mailings'
            },
            width: '100%',
            height: 400,
        }

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        $('#linechart').children().fadeOut(100);
        $('.chartheading').empty();
        $('.inputMonthsRight').removeClass('hide');
        $('.chartheading').append('Customer: ' + cusName);
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', showMailings);

        function showMailings(){
            if(chart.getSelection().length) {
                var clickedRow = chart.getSelection()[0].row;
                var clickedDate = chartData[clickedRow][0];
                var customerId = $('.changeMonthsInput').attr('data-customer');
                console.log(customerId);
                swal({
                    title: "See mailings of " + cusName + " for " + clickedDate + "?",
                    text: "You will leave this page.",
                    type: "warning",
                    showCancelButton: true,
                    closeOnCancel: true,
                    confirmButtonColor: "#4DD0E1",
                    confirmButtonText: "Yes, show me!",

                }, function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "/mailings/c/C" + customerId + "/" + clickedDate;
                    }
                });
            }
        }
    }

    $('#overview_datatable').DataTable({
        "oLanguage": {
            "sLengthMenu": "Show _MENU_"
        },
        "order": [[ 3, "desc" ]]
    });
    $('select').addClass('browser-default');
});