$(function(){

    if (typeof Notification !== 'undefined') {
        if (Notification.permission !== "granted"){
            Notification.requestPermission();
        }

        checkForNotification();

        function checkForNotification(){

            $.ajax({
                url: "/notification",
                type: "GET",
                dataType: "json",
            }).done(function(data){
                if(data != ''){
                    makeNotification(data);
                }
                setTimeout(checkForNotification, 10000);
            }).error(function(data){
                console.log('ERROR', data);
                setTimeout(checkForNotification, 60000);
            });
        };

        function makeNotification(data) {
            var notification = new Notification(data.title, {
                icon: '/img/notificationIcon.png',
                body: data.body,
                sticky: true
            });

            notification.onclick = function () {
                window.open(data.url);
            };
        }
    }

});



