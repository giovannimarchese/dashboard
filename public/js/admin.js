$(function(){
    $('#overview_datatable').DataTable({
        "oLanguage": {
            "sLengthMenu": "Show _MENU_"
        },
        "order": [[ 1, "asc" ]],
    });
    $('.editBtn').html('<i class="material-icons">edit</i>');
    $('.delBtn').html('<i class="material-icons">delete</i>');
    $('select').addClass('browser-default');

    //delete a user
    $('tbody').on("click", ".delBtn",function(e){
        e.preventDefault();
        var target = $(this);
        swal({
            title: "Are you sure?",
            text: "Deleting a user is permanent!",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it.",
            closeOnConfirm: false },
        function(){
            $.ajax({
                url: target.attr('href'),
            })
            .done(function(data){
                swal("Deleted!", "User was deleted", "success");
                target.parent().parent().fadeOut(500);
                console.log(data);

            })
            .error(function(error){
                swal("Error!", "Something went wrong..", "error");
                console.log(error, 'error');
            })
        });
    });
});