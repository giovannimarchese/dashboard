<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'score';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the server that owns the score.
     */
    public function server(){

        return $this->belongsTo('App\Server');
    }

    /**
     * Scope to get latest results first
     * @param $query
     * @return mixed
     */
    public function scopeLatest($query){

        return $query->orderBy('ts', 'desc');
    }
}
