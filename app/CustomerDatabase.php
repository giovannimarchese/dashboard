<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDatabase extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customerdatabase';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get mailings for customer database
     */
    public function mailings(){

        return $this->hasMany('App\Mailing');
    }

    /**
     * Get the customer that owns the database.
     */
    public function customer(){

        return $this->belongsTo('App\Customer');
    }

    public function scopeActive($query){

        return $query->where('active', 'yes');
    }
}
