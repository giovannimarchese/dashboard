<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emarkseed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed database with all data from database server';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->data();
    }

    /**
     * This function seeds the database for the first time
     * It retrieves all customers, customerdatabases & all their mailings
     * After the initial seeding a cron will run daily to handle this.
     */
    private function data(){
        //seeding takes a long time so we cant have it timeout
        set_time_limit(0);
        try {
            //$db_dm = new \PDO('mysql:dbname=dm;host=192.168.56.200;username=emark;password=jari007', 'root', 'jari007');
            $db_dm = new \PDO('mysql:dbname=dm;host=127.0.0.1;port=3307;', 'giovanni', 'g!fh$HgvesDt1');
        } catch (PDOException $ex) {
            echo 'Connection failed: ' . $ex->getMessage();
        }

        $customerRes = $db_dm->query("SELECT * FROM customer");
        $customerRes->setFetchMode(\PDO::FETCH_ASSOC);

        echo '<pre>';
        while ($row = $customerRes->fetch()) {
            $this->comment($row['name'].'----------------insert customer-----------------<<<<');
            DB::table('customer')->insert($row);
        }

        $this->info('Finished inserting customers'."\n");

        $customerDbRes = $db_dm->query("SELECT * FROM customerdatabase");
        $customerDbRes->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('Inserting customersdbs & mailings');
        $this->comment('Get some coffee, this may take a while ;)');
        $bar = $this->output->createProgressBar($customerDbRes->rowCount());
        while ($row = $customerDbRes->fetch()) {
            DB::table('customerdatabase')->insert($row);

            $mailings = $db_dm->query("SELECT * FROM ".$row['db_name'].".mailing");
            if($mailings){
                $mailings->setFetchMode(\PDO::FETCH_ASSOC);
                while ($row_mail = $mailings->fetch()) {
                    $row_mail['old_mailing_id'] = $row_mail['id'];
                    $row_mail['customer_database_id'] = $row['id'];
                    $row_mail['customer_id'] = $row['customer_id'];
                    $row_mail['customer_db'] = $row['db_name'];
                    unset($row_mail['id']);

                    DB::table('mailing')->insert($row_mail);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('Finished seeding database with customer & mailing data!');

    }
}
