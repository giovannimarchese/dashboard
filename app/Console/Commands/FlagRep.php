<?php

namespace App\Console\Commands;

use App\Flagged;
use App\Score;
use App\Setting;
use Illuminate\Console\Command;

class FlagRep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flagrep';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check server reputation and flag according to dashboard settings';

    /**
     * Get dashboard settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->red = Setting::name('red_server')->first();
        $this->orange = Setting::name('orange_server')->first();
        $this->green = Setting::name('green_server')->first();
        $this->date = date('Y-m-d');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        if($this->red->use || $this->orange->use || $this->green->use){
            $flags = $this->checkServerScores();
            $this->setFlags($flags);
        }
    }

    /**
     * check servers if they meet settings standards
     * @return flags
     */
    private function checkServerScores(){

        $scores = Score::where('ts', 'like', $this->date.'%')->get();
        $flags = null;
        foreach($scores as $score){
            if($score->score <= $this->red->var){
                $flags['red'][] = $score;
            } else if($score->score <= $this->orange->var){
                $flags['orange'][] = $score;
            }
        }
        return $flags;
    }

    /**
     * loop through flags and set them
     * @param $flags
     */
    private function setFlags($flags){

        if($flags){
            if(isset($flags['red']) && $this->red->use){
                foreach($flags['red'] as $flag){
                    $this->createFlag($flag->server_id, $this->format_message($this->red->body, $flag->score), 'red');
                }
            }if(isset($flags['orange']) && $this->orange->use){
                foreach($flags['orange'] as $flag){
                    $this->createFlag($flag->server_id, $this->format_message($this->orange->body, $flag->score), 'orange');
                }
            }
        }else if($this->green->use){
            $this->createFlag(0, $this->format_message($this->green->body, false), 'green');
        }
    }

    /**
     * Calculate variable and put it into the msg
     * @param $message
     * @param $score
     * @return message
     */
    private function format_message($message, $score){

        if(!$score){
            $avg = 0;
            $scores = Score::where('ts', 'like', $this->date.'%')->get();
            foreach($scores as $score){
                $avg += $score->score;
            }
            $score = round($avg / $scores->count(),1);
            $message = str_replace('%', $score, $message);
        }else{
            $message = str_replace('%', $score, $message);
        }

        return $message;
    }

    /**
     * create the flags from the giving params
     * @param $server_id
     * @param $message
     * @param $alert
     */
    private function createFlag($server_id, $message ,$alert){

        $flag = new Flagged();
        $flag->user_id = 2;
        $flag->type = 'reputation';
        $flag->foreign_id = $server_id;
        $flag->message = $message;
        $flag->alert = $alert;
        $flag->save();
    }
}
