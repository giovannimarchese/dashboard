<?php

namespace App\Console\Commands;

use App\Flagged;
use App\Notification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MailFlags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailflags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email summary of today\'s flags';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = $this->getSubscribers();
        if($subscriptions){
            $flags = $this->getFlags();
            $this->sendOutFlags($subscriptions, $flags->toArray());
        }
    }

    /**
     * Find all users that want to receive a summary
     * @return subscriptions
     */
    private function getSubscribers(){
        $subscribers = Notification::flagged()->get();

        $subscriptions = [];

        foreach($subscribers as $subscriber)
            if($subscriber->use && ($subscriber->notification == 'mail' || $subscriber->notification == 'both')) array_push($subscriptions, $subscriber);

        return $subscriptions;
    }

    /**
     * find all flags for summary
     * @return flags
     */
    private function getFlags(){

        $dayAgo = substr(Carbon::now()->subHours(24), 0, 13);

        $flags = Flagged::latest()->where('updated_at', '>', $dayAgo.'%')->with('server', 'mailing', 'user')->get();

        return $flags;
    }

    /**
     * Loop through users and send them the flags
     * @param $subscriptions
     * @param $flags
     */
    private function sendOutFlags($subscriptions, $flags){

        foreach($subscriptions as $subscription){

            Mail::send('mails/flags', compact('flags'), function($msg) use ($subscription){
                $msg->from('emark@dashboard.relatify.com', 'Emark Dashboard');
                $msg->to($subscription->user->email, $subscription->user->name.' '.$subscription->user->lastname);
                $msg->subject('Flags of the last 24 hours');
            });

            Notification::where('user_id', $subscription->user->id)->where('type', 'flagged')->update(['last' => $flags[0]['id']]);
        }
    }
}
