<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Senderbase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'senderbase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get reputation data from senderbase.com';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->senderbase();
    }

    /**
     * @throws Exception
     */
    private function senderbase(){

        try{
            $pdo = new \PDO( 'mysql:dbname=' . env('DB_DATABASE') . ';host=' . env('DB_HOST') . ';port=' . env('DB_PORT'),
                env('DB_USERNAME'),
                env('DB_PASSWORD'),
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')
            );

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch( Exception $e ){
            echo $e->getMessage();
            throw $e;
        }

        $stmt = $pdo->prepare('SELECT id, ip FROM server');
        $stmt->execute();
        $ipaddresses = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $sth = $pdo->prepare(
            'INSERT INTO senderbase (
        server_id, reverse_dns, first_seen, email_reputation, magnitude_last_day,
        magnitude_last_month, volume_change, ts
    ) VALUES (
        ?, ?, ?, ?, ?,
        ?, ?, ?
    )'
        );

        $bar = $this->output->createProgressBar(count($ipaddresses));
        $this->comment('Inserting new senderbase');

        foreach( $ipaddresses as $ipaddress ){
            $result = $this->getData( $ipaddress['ip'] );
            $sth->execute(
                array(
                    $ipaddress['id'],
                    $result['reverse_dns'],
                    date('Y-m-d', strtotime( $result['first_seen'] ) ),
                    $result['reputation'],
                    $result['last_day'],
                    $result['last_month'],
                    $result['volume_change'],
                    date( 'Y-m-d H:i:s', time() )
                )
            );
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'Finished inserting new senderbase');
    }


    /**
     * @param $ipaddress
     * @return array
     */
    private function getData( $ipaddress )
    {

        $url = 'http://www.senderbase.org/lookup?search_string=' . $ipaddress;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie');
        curl_setopt($ch, CURLOPT_COOKIEJAR,  '/tmp/cookie');
        curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:25.0) Gecko/20100101 Firefox/25.0'); // empty user agents probably not accepted
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER,    1); // enable this - they check referer on POST

        $html = curl_exec($ch);

        // build array of post values - all are important
        $post = array(
            'tos_accepted' => 'Yes, I Agree');

        // switch request to POST, use http_build_query to encode post data for us
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        $html = curl_exec($ch);

        preg_match( '/<table class="tabular info_table">(.*?)<\/table>/is', $html, $matches );

        $de = preg_replace('/<span [^>]*>(.*?)<\/span>/is', '', $matches[0] );

        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($de);
        libxml_use_internal_errors(false);
        $xpath = new \DOMXPath($doc);

        //echo $de;

        $return = array(
            'first_seen' => '',
            'reverse_dns' => '',
            'reputation' => '',
            'last_day' => '',
            'last_month' => '',
            'volume_change' => ''
        );

        $firstSeenNode = $xpath->query('//tr[3]/td[2]');
        if($firstSeenNode->length > 0){
            $return['first_seen'] = trim($firstSeenNode->item(0)->nodeValue);
        }

        $reverseDNSNode = $xpath->query('//tr[2]/td[2]');
        if($reverseDNSNode->length > 0){
            $return['reverse_dns'] = trim($reverseDNSNode->item(0)->nodeValue);
        }

        //$emailReputationNode = $xpath->query('//tr[8]/td[2]');
        //echo trim( $emailReputationNode->item(0)->nodeValue );

        $emailReputationNode = $xpath->query('//tr[5]/td[2]');
        if($emailReputationNode->length > 0){
            $return['reputation'] = trim( $emailReputationNode->item(0)->nodeValue );
        }

        $emailMagnitudeLastNode = $xpath->query('//tr[8]/td[2]');
        if( $emailMagnitudeLastNode->length > 0 ){
            $return['last_day'] = trim( $emailMagnitudeLastNode->item(0)->nodeValue );
        }

        $emailMagnitudeMonthNode = $xpath->query('//tr[8]/td[3]');
        if( $emailMagnitudeMonthNode->length > 0 ){
            $return['last_month'] = trim( $emailMagnitudeMonthNode->item(0)->nodeValue );
        }

        $emailVolumeChangeNode = $xpath->query('//tr[9]/td[2]');
        if( $emailVolumeChangeNode->length > 0 ){
            $return['volume_change'] = trim( $emailVolumeChangeNode->item(0)->nodeValue );
        }
        return $return;
    }


}
