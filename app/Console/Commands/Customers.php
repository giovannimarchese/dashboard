<?php

namespace App\Console\Commands;

use App\Customer;
use App\Http\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class Customers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get customers & all dbs & all mailings + format it';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getCustomerData();
    }

    /**
     * Get all customers and stats about there mailings
     * After we save these to cache
     * We use this to fill the table on the customer page
     */
    private function getCustomerData()
    {
        $customerData = Customer::all();

        $bar = $this->output->createProgressBar(count($customerData));
        $this->comment('Formatting customer data');

        $count = 0; $num_mails = 0; $num_mailings = 0; $num_reads = 0; $num_clicks = 0; $num_conversions = 0; $num_hb = 0; $num_sb = 0; $num_spc = 0;

        foreach ($customerData as $data) {
            foreach ($data->databases()->active()->get() as $customerDatabase) {
                $num_mailings += $customerDatabase->mailings->count();
                foreach ($customerDatabase->mailings as $mailings) {
                    $num_mails += ($mailings->num_mails - $mailings->remaining_emails);
                    $num_reads += $mailings->reads_unique;
                    $num_clicks += $mailings->clicks_unique;
                    $num_conversions += $mailings->conversions_unique;
                    $num_hb += $mailings->hardbounces;
                    $num_sb += $mailings->softbounces;
                    $num_spc += $mailings->spamcomplaints;
                }
            }
            $customers[$count] = array(
                'id' => $data->id,
                'name' => $data->name,
                'db' => $data->databases->count(),
                'mailings' => $num_mailings,
                'mails' => $num_mails,
                'reads' => Helper::calculatePercentage($num_mails, $num_reads),
                'clicks' => Helper::calculatePercentage($num_mails, $num_clicks),
                'conversions' => Helper::calculatePercentage($num_mails, $num_conversions),
                'hb' => Helper::calculatePercentage($num_mails, $num_hb),
                'sb' => Helper::calculatePercentage($num_mails, $num_sb),
                'spc' => Helper::calculatePercentage($num_mails, $num_spc),

            );
            $count++; $num_mails = 0; $num_mailings = 0; $num_reads = 0; $num_clicks = 0; $num_conversions = 0; $num_hb = 0; $num_sb = 0; $num_spc = 0;
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'Finished formatting customer data');
        Cache::forget('customers');
        Cache::forever('customers', $customers);
        $this->info("\n".'Saved data to cache');
    }
}
