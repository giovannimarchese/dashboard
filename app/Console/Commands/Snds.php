<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Snds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get reputation data from postmaster.live.com - SNDS';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->snds();
    }

    /**
     * Get snds
     */
    private function snds(){
        try{
            $pdo = new \PDO( 'mysql:dbname=' . env('DB_DATABASE') . ';host=' . env('DB_HOST') . ';port=' . env('DB_PORT'),
                env('DB_USERNAME'),
                env('DB_PASSWORD'),
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')
            );

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch( Exception $e ){
            echo $e->getMessage();
        }

        $sthIp = $pdo->prepare('SELECT id FROM server WHERE ip = ? LIMIT 1');

        $sthSNDS = $pdo->prepare(
            'INSERT INTO snds (server_id, `from`, till, rcpt, `data`, msg_rcpt, filter, complaint_rate, from_trap, till_trap, trap, helo)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

        $old = 'https://postmaster.live.com/snds/data.aspx?key=77029611-4d8f-499d-b5dd-c2abcf021486';
        $new = 'https://postmaster.live.com/snds/data.aspx?key=8f2dca7b-398b-477e-895a-ac38a3a05b95';
        $this->comment('Opening csv and inserting new SNDS');
        if (($handle = fopen($new, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $sthIp->execute(array($data[0]));
                $ips = $sthIp->fetch(\PDO::FETCH_ASSOC);

                if($data[10] == 0){
                    $data[8] = null;
                    $data[9] = null;
                }

                $sthSNDS->execute(array(
                    $ips['id'], date( 'Y-m-d H:i:s', strtotime($data[1]) ), date( 'Y-m-d H:i:s', strtotime($data[2]) ),
                    $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[11]
                ));
            }
            fclose($handle);
        }
        $this->info('Finished inserting new SNDS');
    }
}
