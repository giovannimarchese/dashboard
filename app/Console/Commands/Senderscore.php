<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Senderscore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'senderscore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get reputation data from senderscore.com';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->senderscore();
    }

    /**
     * Get senderscore
     */
    private function senderscore(){
        try{
            $pdo = new \PDO( 'mysql:dbname=' . env('DB_DATABASE') . ';host=' . env('DB_HOST') . ';port=' . env('DB_PORT'),
                env('DB_USERNAME'),
                env('DB_PASSWORD'),
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')
            );

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch( Exception $e ){
            echo $e->getMessage();
        }

        $stmt = $pdo->prepare('SELECT id, ip FROM server');
        $stmt->execute();

        $insert = $pdo->prepare( 'INSERT INTO score (server_id, ts, score) VALUES (?,?,?)' );

        $ipaddresses = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $testDomain = 'score.senderscore.com';

        $bar = $this->output->createProgressBar(count($ipaddresses));
        $this->comment('Inserting new senderscores');

        foreach( $ipaddresses as $ipaddress ){
            $arrIp = explode('.', $ipaddress['ip']);
            $arrIp = array_reverse($arrIp);
            $ip = implode('.', $arrIp);

            $records = dns_get_record($ip.'.'.$testDomain, DNS_A);

            if(count($records) > 0){
                $record = $records[0];
                if(isset($record['ip'])){
                    $arr = explode('.',$record['ip']);
                    $reputation = $arr[3];
                    $insert->execute(array($ipaddress['id'], date( 'Y-m-d H:i:s', time()), $reputation));
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'Finished inserting new senderscores');
    }


}
