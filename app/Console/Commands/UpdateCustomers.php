<?php

namespace App\Console\Commands;

use App\Customer;
use App\CustomerDatabase;
use Illuminate\Console\Command;

class UpdateCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emarkcustomers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all customers & databases';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $db_dm = new \PDO('mysql:dbname=dm;host=127.0.0.1;port=3307;', 'giovanni', 'g!fh$HgvesDt1');
        } catch (PDOException $ex) {
            echo 'Connection failed: ' . $ex->getMessage();
        }

        $this->updateCustomers($db_dm);
        $this->updateDatabases($db_dm);
    }

    /**
     * Loop through all customers and update them
     * @param $db_dm
     */
    private function updateCustomers($db_dm){

        $customers = $db_dm->query("SELECT * FROM customer");
        $customers->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('updating customer information');

        $bar = $this->output->createProgressBar($customers->rowCount());

        while ($row = $customers->fetch()) {
            Customer::where('id', $row['id'])->update($row);

            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'finished updating customers');
    }

    /**
     * Loop through all databases and update them
     * @param $db_dm
     */
    private function updateDatabases($db_dm){

        $databases = $db_dm->query("SELECT * FROM customerdatabase");
        $databases->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('updating database information');

        $bar = $this->output->createProgressBar($databases->rowCount());

        while ($row = $databases->fetch()) {
            CustomerDatabase::where('id', $row['id'])->update($row);

            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'finished updating databases');
    }
}
