<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Emarksubs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emarksubs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '512M');

        $this->info('Running schedule to update subscriber data🚀 ');
        $this->info('This make take a while, take a piss or get some coffee☕️ ..' . "\n");

        try {
            $db_dm = new \PDO('mysql:dbname=dm;host=127.0.0.1;port=3307;', 'giovanni', 'g!fh$HgvesDt1');
        } catch (PDOException $ex) {
            echo 'Connection failed: ' . $ex->getMessage();
        }

        $this->getSpam($db_dm);
        $this->getHB($db_dm);

        $this->info("\n".'Finished !');
    }

    private function getSpam($db_dm){

        $cusDb = $db_dm->query("SELECT * FROM customerdatabase WHERE active = 'yes'");
        $cusDb->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment("\n" . 'Looping through customerdatabases to find new spamcomplaints');
        $bar = $this->output->createProgressBar($cusDb->rowCount());
        while ($row = $cusDb->fetch()) {

            $current_ids = DB::table('spamcomplaint')->where('customer_database_id', $row['id'])->lists('old_spam_id');

            $subscribers = $db_dm->query("  SELECT sa.email as subscriber, s.id, s.ts, s.mailing_id
                                            FROM ".$row['db_name'].".spamcomplaint as s
                                            INNER JOIN ".$row['db_name'].".subscriber_archive as sa ON s.subscriber_id = sa.id
                                            WHERE s.id NOT IN('".implode($current_ids,"', '")."')");
                                            //WHERE s.ts BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)) AND DATE(NOW())

            if($subscribers){
                $subscribers->setFetchMode(\PDO::FETCH_ASSOC);

                while ($row_subs = $subscribers->fetch()) {
                    $row_subs['old_spam_id'] = $row_subs['id'];
                    $row_subs['customer_database_id'] = $row['id'];
                    $row_subs['customer_id'] = $row['customer_id'];
                    unset($row_subs['id']);

                    DB::table('spamcomplaint')->insert($row_subs);
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }

    private function getHB($db_dm){

        $cusDb = $db_dm->query("SELECT * FROM customerdatabase WHERE active = 'yes'");
        $cusDb->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment("\n" . 'Looping through customerdatabases to find new hardbounces');
        $bar = $this->output->createProgressBar($cusDb->rowCount());
        while ($row = $cusDb->fetch()) {

            $current_ids = DB::table('hardbounce')->where('customer_database_id', $row['id'])->lists('old_hb_id');

            $subscribers = $db_dm->query("  SELECT sa.email as subscriber, h.id, h.ts, h.mailing_id, h.smtp_code, h.smtp_status
                                            FROM ".$row['db_name'].".hardbounce as h
                                            INNER JOIN ".$row['db_name'].".subscriber_archive as sa ON h.subscriber_id = sa.id
                                            WHERE h.id NOT IN('".implode($current_ids,"', '")."')");

            if($subscribers){
                $subscribers->setFetchMode(\PDO::FETCH_ASSOC);

                while ($row_subs = $subscribers->fetch()) {
                    $row_subs['old_hb_id'] = $row_subs['id'];
                    $row_subs['customer_database_id'] = $row['id'];
                    $row_subs['customer_id'] = $row['customer_id'];
                    unset($row_subs['id']);

                    DB::table('hardbounce')->insert($row_subs);
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
