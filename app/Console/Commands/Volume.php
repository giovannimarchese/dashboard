<?php

namespace App\Console\Commands;

use App\Mailing;
use App\Http\Helper;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class Volume extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'volumes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate volumes and store to cache';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->calculateVolume();
    }

    /**
     * Calculate different volume totals and save them to cache
     */
    private function calculateVolume(){

        $dates = [ 'thisYear' => date('Y'), 'lastMonth' => substr(Carbon::parse()->subMonth(1), 0, 7), 'thisMonth' => date('Y-m'), 'today' => date('Y-m-d') ];
        $allTime = [ 'total' => 0, 'adjusted' => 0, 'clicks' => 0, 'reads' => 0, 'conversions' => 0, 'softbounces' => 0, 'hardbounces' => 0, 'spam' => 0 ];
        $thisYear = [ 'total' => 0, 'adjusted' => 0 ];
        $lastMonth = [ 'total' => 0, 'adjusted' => 0 ];
        $thisMonth = [ 'total' => 0, 'adjusted' => 0 ];
        $today = [ 'total' => 0, 'adjusted' => 0 ];

        Mailing::completed()->chunk(250, function($mailings) use (&$allTime, &$thisYear, &$lastMonth, &$thisMonth, &$today, $dates){
            foreach($mailings as $mailing){
                $temp = $mailing->num_mails - $mailing->hardbounces;
                $allTime['total'] += $mailing->num_mails;
                $allTime['adjusted'] += $temp;
                $allTime['reads'] += $mailing->reads_unique;
                $allTime['clicks'] += $mailing->clicks_unique;
                $allTime['conversions'] += $mailing->conversions_unique;
                $allTime['hardbounces'] += $mailing->hardbounces;
                $allTime['spam'] += $mailing->spamcomplaints;
                if ($dates['thisYear'] == intval(substr($mailing->starttime, 0, 4))){
                    $temp = $mailing->num_mails - $mailing->hardbounces;
                    $thisYear['total'] += $mailing->num_mails;
                    $thisYear['adjusted'] += $temp;
                } if ($dates['lastMonth'] == substr($mailing->starttime, 0, 7)){
                    $temp = $mailing->num_mails - $mailing->hardbounces;
                    $lastMonth['total'] += $mailing->num_mails;
                    $lastMonth['adjusted'] += $temp;
                } else if ($dates['thisMonth'] == substr($mailing->starttime, 0, 7)){
                    $temp = $mailing->num_mails - $mailing->hardbounces;
                    $thisMonth['total'] += $mailing->num_mails;
                    $thisMonth['adjusted'] += $temp;
                    if ($dates['today'] == substr($mailing->starttime, 0, 10)){
                        $today['total'] += $mailing->num_mails;
                    }
                }
            }
        });

        $volumes = array(
            'allTime' => [
                'total' => number_format($allTime['total'], 0, ',', ' '),
                'delivered' => number_format($allTime['adjusted'], 0, ',', ' '),
                'del_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['adjusted']),
                'reads' => number_format($allTime['reads'], 0, ',', ' '),
                'read_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['reads']),
                'clicks' => number_format($allTime['clicks'], 0, ',', ' '),
                'click_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['clicks']),
                'conversions' => number_format($allTime['conversions'], 0, ',', ' '),
                'conv_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['conversions']),
                'hardbounces' => number_format($allTime['hardbounces'], 0, ',', ' '),
                'hb_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['hardbounces']),
                'spam' => number_format($allTime['spam'], 0, ',', ' '),
                'spam_percentage' => Helper::calculatePercentage($allTime['total'], $allTime['spam']),
            ],
            'thisYear' => [
                'total' => number_format($thisYear['total'], 0, ',', ' '),
                'delivered' => Helper::calculatePercentage($thisYear['total'], $thisYear['adjusted'])
            ],
            'lastMonth' => [
                'total' => number_format($lastMonth['total'], 0, ',', ' '),
                'delivered' => Helper::calculatePercentage($lastMonth['total'], $lastMonth['adjusted'])
            ],
            'thisMonth' => [
                'total' => number_format($thisMonth['total'], 0, ',', ' '),
                'delivered' => Helper::calculatePercentage($thisMonth['total'], $thisMonth['adjusted'])
            ],
            'today' => [
                'total' => number_format($today['total'], 0, ',', ' ')
            ],
        );
        Cache::forget('volumes');
        Cache::forever('volumes', $volumes);
        $this->info('Volumes where calculated and the volume cache was renewed');

    }
}
