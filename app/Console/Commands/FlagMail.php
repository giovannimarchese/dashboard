<?php

namespace App\Console\Commands;

use App\Http\Helper;
use App\Mailing;
use App\Setting;
use App\Flagged;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FlagMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flagmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check mailings and flag according to dashboard settings';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->red = Setting::name('red_mailing')->first();
        $this->orange = Setting::name('orange_mailing')->first();
        $this->minimum = Setting::name('min_mails')->first();
        $this->date = Carbon::now()->subHours(Setting::name('update_hours')->first()->var);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->red->use || $this->orange->use) {
            $flags = $this->checkMailings();
            $this->setFlags($flags);
        }
    }

    /**
     * check mailings if they meet settings standards
     * @return flags
     */
    private function checkMailings(){

        $mailings = Mailing::where('starttime', '>=', $this->date)->mails($this->minimum->var)->latest()->completed()->get();
        $flags = null;
        foreach($mailings as $mailing){
            $percentage = Helper::calculateDeliverability($mailing->num_mails, $mailing->hardbounces);
            if($percentage <= $this->red->var){
                $mailing->percentage = $percentage;
                $flags['red'][] = $mailing;
            }else if($percentage <= $this->orange->var){
                $mailing->percentage = $percentage;
                $flags['orange'][] = $mailing;
            }
        }
        return $flags;
    }

    /**
     * loop through flags and set them
     * @param $flags
     */
    private function setFlags($flags){

        if($flags){
            if(isset($flags['red']) && $this->red->use){
                foreach($flags['red'] as $flag){
                    $this->createFlag($flag->id, str_replace('%', $flag->percentage.'%', $this->red->body), 'red');
                }
            }if(isset($flags['orange']) && $this->orange->use){
                foreach($flags['orange'] as $flag){
                    $this->createFlag($flag->id, str_replace('%', $flag->percentage.'%', $this->orange->body), 'orange');
                }
            }
        }
    }

    /**
     * create the flags from the giving params
     * @param $mailing_id
     * @param $message
     * @param $alert
     */
    private function createFlag($mailing_id, $message ,$alert){

        if(!$flag = Flagged::where('type', 'mailing')->where('foreign_id', $mailing_id)->first()){
            $flag = new Flagged();
            $flag->user_id = 2;
            $flag->type = 'mailing';
            $flag->foreign_id = $mailing_id;
        }
        $flag->message = $message;
        $flag->alert = $alert;
        $flag->save();
    }
}
