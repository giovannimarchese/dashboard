<?php

namespace App\Console\Commands;

use App\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Emarkmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emarkmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get new customer/mailing data from database server';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->days = Setting::name('update_days')->first()->var;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running schedule to update dashboard data🚀 ');
        $this->info('This make take a while, take a piss or get some coffee☕️ ..'."\n");

        try {
            $db_dm = new \PDO('mysql:dbname=dm;host=127.0.0.1;port=3307;', 'giovanni', 'g!fh$HgvesDt1');
        } catch (PDOException $ex) {
            echo 'Connection failed: ' . $ex->getMessage();
        }

        $this->newCustomers($db_dm);
        $this->newDatabases($db_dm);
        $this->updateMailings($db_dm);
        $this->newMailings($db_dm);

        $this->info('Dashboard data is now up to date!😎');
    }

    /**
     * Search for and insert new customers into customer table
     * @param $db_dm
     */
    private function newCustomers($db_dm){

        $current_cus_ids = DB::table('customer')->lists('id');

        $newCus = $db_dm->query("SELECT * FROM customer WHERE id NOT IN ( '".implode($current_cus_ids,"', '")."')");
        $newCus->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('Checking for new customers');

        while ($row = $newCus->fetch()) {
            $this->comment('Inserting new customer');
            DB::table('customer')->insert($row);
        }

        if($newCus->rowCount() == 0){
            $this->error('No new customers found😱 ');
        }else{
            $this->info('Finished inserting new customers✔️');
        }
        $this->info('Moooo🐮 ving on..'."\n");
    }

    /**
     * Search for and insert new databases into customerdatabase table
     * @param $db_dm
     */
    private function newDatabases($db_dm){

        $current_cus_ids = DB::table('customerdatabase')->lists('id');

        $newCus = $db_dm->query("SELECT * FROM customerdatabase WHERE id NOT IN ( '".implode($current_cus_ids,"', '")."')");
        $newCus->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('Checking for new customerdatabases');

        while ($row = $newCus->fetch()) {
            $this->comment('Inserting new database');
            DB::table('customerdatabase')->insert($row);
        }

        if($newCus->rowCount() == 0){
            $this->error('No new databases found😱 ');
        }else{
            $this->info('Finished inserting new databases✔️');
        }
        $this->info('Moooo🐮 ving on..'."\n");
    }

    /**
     * select all mailings withing settings range and update these
     * @param $db_dm
     */
    private function updateMailings($db_dm){

        $cusDb = $db_dm->query("SELECT * FROM customerdatabase WHERE active = 'yes'");
        $cusDb->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('Looping through customerdatabases to update mailings');
        $bar = $this->output->createProgressBar($cusDb->rowCount());
        while ($row = $cusDb->fetch()) {

            $updatedate = Carbon::now()->subDays($this->days);

            $current_cus_ids = DB::table('mailing')->where('customer_database_id', $row['id'])->where('ts', '>', substr($updatedate,0,10))->lists('old_mailing_id');

            $mailings = $db_dm->query("SELECT * FROM ".$row['db_name'].".mailing WHERE id IN('".implode($current_cus_ids,"', '")."')");
            if($mailings){
                $mailings->setFetchMode(\PDO::FETCH_ASSOC);

                while ($row_mail = $mailings->fetch()) {
                    $row_mail['old_mailing_id'] = $row_mail['id'];
                    $row_mail['customer_database_id'] = $row['id'];
                    $row_mail['customer_id'] = $row['customer_id'];
                    $row_mail['customer_db'] = $row['db_name'];
                    unset($row_mail['id']);

                    DB::table('mailing')
                        ->where('old_mailing_id', $row_mail['old_mailing_id'])
                        ->where('customer_database_id', $row['id'])
                        ->update($row_mail);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'Finished updating mailings✔️');
        $this->info('Moooo🐮 ving on..'."\n");
    }

    /**
     * find new mailings and insert them into mailings table
     * @param $db_dm
     */
    private function newMailings($db_dm){

        $cusDb = $db_dm->query("SELECT * FROM customerdatabase WHERE active = 'yes'");
        $cusDb->setFetchMode(\PDO::FETCH_ASSOC);

        $this->comment('Looping through customerdatabases to find new mailings');
        $bar = $this->output->createProgressBar($cusDb->rowCount());
        while ($row = $cusDb->fetch()) {

            $current_ids = DB::table('mailing')->where('customer_database_id', $row['id'])->lists('old_mailing_id');

            $mailings = $db_dm->query("SELECT * FROM ".$row['db_name'].".mailing WHERE id NOT IN('".implode($current_ids,"', '")."')");
            if($mailings){
                $mailings->setFetchMode(\PDO::FETCH_ASSOC);

                while ($row_mail = $mailings->fetch()) {
                    $row_mail['old_mailing_id'] = $row_mail['id'];
                    $row_mail['customer_database_id'] = $row['id'];
                    $row_mail['customer_id'] = $row['customer_id'];
                    $row_mail['customer_db'] = $row['db_name'];
                    unset($row_mail['id']);

                    DB::table('mailing')->insert($row_mail);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n".'Finished inserting new mailings✔️');
        $this->info('Moooo🐮 ving on..'."\n");
    }
}
