<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //\App\Console\Commands\Seed::class,
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\Senderscore::class,
        \App\Console\Commands\Senderbase::class,
        \App\Console\Commands\Snds::class,
        \App\Console\Commands\Customers::class,
        \App\Console\Commands\Emarkmail::class,
        \App\Console\Commands\Emarksubs::class,
        \App\Console\Commands\Volume::class,
        \App\Console\Commands\FlagRep::class,
        \App\Console\Commands\FlagMail::class,
        \App\Console\Commands\UpdateCustomers::class,
        \App\Console\Commands\MailFlags::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //sent email to user with flag subscription
        $schedule->command('mailflags')->dailyAt('09:00');

        //update dashboard data (customers, customerdatabases, mailings)
        $schedule->command('emarkmail')->everyThirtyMinutes();
        //flag mailings
        $schedule->command('flagmail')->everyThirtyMinutes();
        //update volume data cache
        $schedule->command('volume')->hourly();
        //update subscriber data
        $schedule->command('emarksubs')->hourly();
        //update customer & database data
        $schedule->command('emarkcustomers')->hourly();

        //get all reputation scores/data
        $schedule->command('senderscore')->dailyAt('16:10');
        $schedule->command('senderbase')->dailyAt('16:10');
        $schedule->command('snds')->dailyAt('16:10');
        //flag reputation
        $schedule->command('flagrep')->dailyAt('16:15');

        //cache data for customertable on customerpage
        $schedule->command('customers')->dailyAt('01:00');

    }
}
