<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notification';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id'];

    /**
     * Get the db that owns the notifcation.
     */
    public function user(){

        return $this->belongsTo('App\User');
    }

    /**
     * Scope to get results where type is flagged
     * @param $query
     * @return mixed
     */
    public function scopeFlagged($query){

        return $query->where('type', 'flagged');
    }
}
