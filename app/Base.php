<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'senderbase';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the server that owns the score.
     */
    public function server(){

        return $this->belongsTo('App\Server');
    }
}
