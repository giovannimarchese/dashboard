<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'domain';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Scope to get all every row
     * Where we have at least 1 deferred
     * @param $query
     * @return mixed
     */
    public function scopeDeferred($query){

        return $query->where('deferred', '!=', '0');
    }
}
