<?php

namespace App\Providers;

use App\Setting;
use App\Server;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->getUserNotification();
        $this->checkUserNotification();
        $this->getUserInitials();
        $this->serversForFlagForm();
        $this->getRepScore();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get notification details for user
     */
    private function getUserNotification(){

        view()->composer(['partials/notifications'], function($view){
            $view->with('notification', Notification::where('user_id', Auth::user()->id)->first());
        });
    }

    /**
     * Gets notification status for user
     */
    private function checkUserNotification(){

        view()->composer(['app'], function ($view) {
            $notification = false;
            if(Auth::check()){
                if(Notification::where('user_id', Auth::user()->id)->where('use', 1)->where('notification', 'both')->orWhere('notification', 'desktop')->first())
                    $notification = true;
            }
            $view->with('notification', $notification);
        });
    }

    /**
     * Generate users initials for the navbar
     */
    private function getUserInitials(){

        view()->composer(['header', 'pages/account'], function ($view) {
            if(Auth::check()){
                $initials = strtoupper(substr(Auth::user()->name, 0, 1).substr(Auth::user()->lastname, 0, 1));

                $view->with('initials', $initials);
            }
        });
    }

    /**
     * Pass all servers to the flag reputation form
     */
    private function serversForFlagForm(){

        view()->composer(['partials/repForm'], function($view){
            $view->with('servers', Server::all());
        });
    }

    /**
     * Get color levels for reputation table
     */
    private function getRepScore(){

        view()->composer(['pages/reputation', 'pages/home'], function($view){

            $red = Setting::name('red_server')->first()->var;
            $orange = Setting::name('orange_server')->first()->var;

            $view->with('levels', compact('red', 'orange'));
        });
    }
}
