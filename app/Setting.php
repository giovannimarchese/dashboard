<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Scope to get a pretty syntax
     * Getting setting by name
     * @param $query
     * @param $name
     */
    public function scopeName($query, $name){
        $query->where('name', $name);
    }
}
