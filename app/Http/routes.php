<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/home');
    }
    return redirect('/login');
});

Route::get('/login', function(){
   return redirect('/auth/login');
});

// Authentication routes...
Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');

//You can only access these routes if authenticated
Route::group(['middleware' => 'auth'], function () {
    // Application routes...
    Route::get('/home', 'HomeController@index');
    //reputation
    Route::get('/reputation', 'ReputationController@index');
    Route::get('/reputation/{days}', 'ReputationController@getGraph');
    Route::get('/reputation/date/{date}', 'ReputationController@getReputation');
    //traffic
    Route::get('/traffic', 'TrafficController@index');
    Route::post('/traffic/line', 'TrafficController@getLineChartForDate');
    Route::post('/traffic/pie', 'TrafficController@getPieChartForDate');
    //volume
    Route::get('/volume', 'VolumeController@index');
    Route::post('/volume/dates', 'VolumeController@getLineChartForDates');
    //customers
    Route::get('/customers', 'CustomerController@index');
    Route::post('/customers', 'CustomerController@mailings');
    //mailings (AJAX datatable)
    Route::get('/mailings/table/{month?}', 'MailingController@table');
    Route::get('/mailings/d/table/{date?}', 'MailingController@dateTable');
    Route::get('/mailings/c/table/{id}/{date?}', 'MailingController@customerTable');
    //mailings (load views)
    Route::get('/mailings/{month?}', 'MailingController@index');
    Route::get('/mailings/d/{date?}', 'MailingController@mailingsForDate');
    Route::get('/mailings/c/{id}/{date?}', 'MailingController@mailingsForCustomer');
    //mailings (AJAX selectbox & barchart)
    Route::post('/mailings', 'MailingController@getDatabases');
    Route::post('/mailings/bar', 'MailingController@getBarData');
    //mailings (AJAX hardbounce & spamcomplaints)
    Route::get('/mailings/hb/{mailing}/{database}', 'MailingController@hardbounce');
    Route::get('/mailings/spam/{mailing}/{database}', 'MailingController@spam');
    //domains
    Route::get('/domains/{date?}', 'DomainController@index');
    Route::get('/domains/table/{date}', 'DomainController@table');
    Route::post('/domains/pie', 'DomainController@pieChartData');
    //flagged
    Route::get('/flagged', 'FlaggedController@index');
    Route::post('/flagged', 'FlaggedController@flag');
    Route::get('/flagged/load/{offset}', 'FlaggedController@loadMore');
    //users
    Route::get('/account', 'UserController@index');
    Route::post('/account/{id}', 'UserController@update');
    //notification
    Route::get('/notification', 'NotificationController@checkForNotification');
    Route::post('/notification', 'NotificationController@changeNotification');
    //You can only access these routes if administrator
    Route::group(['middleware' => 'admin'], function () {
        //admin panel
        Route::get('/admin', 'AdminController@index');
        //register user
        Route::get('/admin/register', 'AdminController@newUser');
        Route::post('/admin/register', 'AdminController@registerUser');
        //maintain users
        Route::get('/admin/users', 'AdminController@allUsers');
        Route::get('/admin/edit/{id}', 'AdminController@editUser');
        Route::post('/admin/update', 'AdminController@updateUser');
        Route::get('/admin/delete/{id}', 'AdminController@deleteUser');
        //settings
        Route::get('/admin/settings', 'SettingController@index');
        Route::post('/admin/settings', 'SettingController@update');
        //server
        Route::get('/admin/server', 'AdminController@newServer');
        Route::post('/admin/server', 'AdminController@addServer');
    });
});
