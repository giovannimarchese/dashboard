<?php

namespace App\Http;

class Helper {

    /**
     * calculates the percentage of a total with a part
     * if the percentage is smaller then 0.01% OR
     * if the total is 0 (cant divide by zero)
     * we return the part instead of the percentage
     * if a percentage is above 100 we substr it till the dot
     * @param $total int
     * @param $part int
     * @return int
     */
    public static function calculatePercentage($total, $part){
        if($total == 0){
            return $part;
        } else if($part > 0){
            $a = $total / 100;
            $b = $part / $a;
            if($b < 0.01){
                return '0.01%';
            }else if($b > 100){
                $pos = strpos($b, '.');
                if($pos) return substr($b, 0, $pos).'%';
            }
            return substr($b, 0, 4).'%';
        }
        return $part;
    }

    /**
     * calculate deferred percentage
     * if percentage is smaller then 0.01
     * return >0.01 instead
     * @param $sent int
     * @param $deferred int
     * @return string
     */
    public static function calculateDeferred($sent, $deferred){
        $total = $sent + $deferred;
        $a = $total / 100;
        $b = $deferred / $a;
        if($b < 0.01) return '> 0.01%';
        return substr($b, 0, 4).'%';
    }

    /**
     * Transform a large number into a written number
     * @param $number
     * @return bool|string
     */
    public static function numberToNiceString($number){

        if(!is_numeric($number)) return false;

        if($number>1000000000000) return round(($number/1000000000000),1).' trillion';
        else if($number>1000000000) return round(($number/1000000000),1).' billion';
        else if($number>1000000) return round(($number/1000000),1).' million';
        else if($number>1000) return round(($number/1000)).' thousand';

        return number_format($number);
    }

    /**
     * Calculate deliverability percentage
     * @param $total
     * @param $hb
     * @return float
     */
    public static function calculateDeliverability($total, $hb){
        $a = $total / 100;
        $b = $hb / $a;
        $c = 100 - $b;
        return round($c, 1);
    }
}