<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Http\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainController extends Controller
{
    /**
     * Show domains table for a specific date
     * @param string $date
     * @return view
     */
    public function index($date = 'yesterday'){

        if($date != 'yesterday') {
            if (!\DateTime::createFromFormat('Y-m-d', $date) || strlen($date) != 10)
                return back()->with('flash_message', 'This is not a valid date');
        }else{
            $date = substr(Carbon::yesterday(), 0, 10);
        }

        return view('pages/domain', compact('date'));
    }

    /**
     * Get and format data for domains table
     * @param $date
     * @return array
     */
    public function table($date){

        if($date == 'yesterday') $date = substr(Carbon::yesterday(), 0, 10);

        $domainsData = Domain::where('date', $date)->deferred()->get();

        $domains = array();

        foreach($domainsData as $domain){
            if(isset($domains[$domain->domain])){
                $domains[$domain->domain]['sent'] += $domain->sent;
                $domains[$domain->domain]['deferred'] += $domain->deferred;
            }else{
                $domains[$domain->domain]['domain'] = $domain->domain;
                $domains[$domain->domain]['sent'] = $domain->sent;
                $domains[$domain->domain]['deferred'] = $domain->deferred;
            }
        }
        $count = 0;
        foreach($domains as $domain){
            $formattedDomains[$count] = array(
                'domain' => $domain['domain'],
                'sent' => $domain['sent'],
                'deferred' => $domain['deferred'],
                'percentage' => Helper::calculateDeferred($domain['sent'], $domain['deferred'])
            );
            $count++;
        }

        if(!isset($formattedDomains)) return ["data" => []];

        return ["data" => $formattedDomains];

    }

    /**
     * Get and format data for piechart
     * When you click a row we generate data for this domain
     * @param Request $request
     * @return array
     */
    public function pieChartData(Request $request){

        $date = $request->input('date');
        $name = $request->input('name');

        $domainData = Domain::where('date', $date)->where('domain', $name)->get();

        $sentDeferred = array();
        foreach($domainData as $domain){
            if(isset($sentDeferred['sent'])){
                $sentDeferred['sent'] += $domain->sent;
                $sentDeferred['deferred'] += $domain->deferred;
            }else{
                $sentDeferred['sent'] = $domain->sent;
                $sentDeferred['deferred'] = $domain->deferred;
            }
        }

        $deferredServer = array();
        foreach($domainData as $domain){
            $deferredServer[$domain->server]['deferred'] = $domain->deferred;
        }

        $sentServer = array();
        foreach($domainData as $domain){
            $sentServer[$domain->server]['server'] = $domain->server;
            $sentServer[$domain->server]['sent'] = $domain->sent;
        }

        $pieData = array(
            'mixed' => $sentDeferred,
            'deferred' => $deferredServer,
            'sent' => $sentServer
        );

        return $pieData;
    }
}
