<?php

namespace App\Http\Controllers;

use App\Mailing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class VolumeController extends Controller
{
    /**
     * Load volume view and use volume cache
     * For volume numbers
     * @return view
     */
    public function index(){

        $volume = Cache::get('volumes');

        $weekAgo = substr(Carbon::now()->subDays(7), 0, 10);

        return view('pages/volume', compact('volume', 'weekAgo'));
    }

    /**
     * Get the volume for between 2 dates
     * So we can populate a linechart
     * @param Request $request
     * @return array
     */
    public function getLineChartForDates(Request $request)
    {
        $date1 = $request->input('date1');
        $date2 = substr(Carbon::createFromFormat('Y-m-d', $request->input('date2'))->addDay(), 0, 10);

        $num_mails = [];
        $reads = [];
        $clicks = [];
        $conversions = [];
        $softbounces = [];
        $hardbounces = [];

        Mailing::where('starttime', '>=', $date1)->where('starttime', '<', $date2)->completed()->oldest()
            ->chunk(3000, function($volume) use (&$num_mails, &$reads, &$clicks, &$conversions, &$softbounces, &$hardbounces){
            foreach ($volume as $vol) {
                $date = substr($vol->starttime, 0, 10);
                if (!isset($num_mails[$date])) {
                    $num_mails[$date] = $vol->num_mails;
                    $reads[$date] = $vol->reads_unique;
                    $clicks[$date] = $vol->clicks_unique;
                    $conversions[$date] = $vol->conversions_unique;
                    $softbounces[$date] = $vol->softbounces;
                    $hardbounces[$date] = $vol->hardbounces;
                } else {
                    $num_mails[$date] += $vol->num_mails;
                    $reads[$date] += $vol->reads_unique;
                    $clicks[$date] += $vol->clicks_unique;
                    $conversions[$date] += $vol->conversions_unique;
                    $softbounces[$date] += $vol->softbounces;
                    $hardbounces[$date] += $vol->hardbounces;
                }
            }
        });

        $volume = array(
            'total' => $num_mails,
            'reads' => $reads,
            'clicks' => $clicks,
            'conversions' => $conversions,
            'softbounces' => $softbounces,
            'hardbounces' => $hardbounces
        );

        return $volume;
    }
}
