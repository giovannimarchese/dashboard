<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use App\Score;
use App\Server;
use App\Mailing;
use App\Flagged;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show homepage
     * @return view
     */
    public function index(){

        $reputation = $this->reputation();
        $volume = $this->volume();
        $mailings = Mailing::latest()->completed()->take(10)->get();
        $flags = Flagged::latest()->take(10)->get();

        return view('pages/home', compact('reputation', 'volume', 'mailings', 'flags'));
    }

    /**
     * Make a collection with all rep scores of today
     * We use a collection so we can use funcs
     * Like max, min, avg in the view
     * @return collection
     */
    private function reputation(){

        $scores = Score::latest()->take(Server::all()->count())->get();

        $reputation = collect([]);

        foreach($scores as $score){
            $reputation->push($score->score);
        }

        return $reputation;
    }

    /**
     * Get mail volume of today & yesterday
     * @return array
     */
    private function volume(){

        $mailings = Mailing::where('starttime', 'like', date('Y-m-d').'%')->completed()->get();

        $volume['today'] = 0;
        foreach($mailings as $mailing){
            $volume['today'] += $mailing->num_mails;
        }

        $date = substr(Carbon::yesterday(), 0, 10);
        $mailings = Mailing::where('starttime', 'like', $date.'%')->completed()->get();

        $volume['yesterday'] = 0;
        foreach($mailings as $mailing){
            $volume['yesterday'] += $mailing->num_mails;
        }

        $volume['today'] = Helper::numberToNiceString($volume['today']);
        $volume['yesterday'] = Helper::numberToNiceString($volume['yesterday']);

        return $volume;
    }

}
