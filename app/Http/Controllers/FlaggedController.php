<?php

namespace App\Http\Controllers;

use App\Flagged;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FlaggedController extends Controller
{
    /**
     * Load flagsfeed with latest flags
     * @return view
     */
    public function index(){

        $flags = Flagged::latest()->take(5)->get();

        return view('pages/flagged', compact('flags'));
    }

    /**
     * Create a flag from post
     * @param Request $request
     * @return string
     */
    public function flag(Request $request){

        $flag = new Flagged;
        $flag->user_id = $request->input('user_id');
        $flag->type = $request->input('type');
        $flag->foreign_id = $request->input('foreign_id');
        $flag->alert = $request->input('alert');
        $flag->message = $request->input('message');
        $flag->save();

        return json_encode('saved');
    }

    /**
     * Load more flags and append to flagsfeed
     * @param $offset
     * @return view
     */
    public function loadMore($offset){

        $flags = Flagged::latest()->take(5)->skip($offset)->get();

        if(!empty($flags->toArray())){
            return view('partials/flags', compact('flags'));
        }else{
            return '';
        }

    }
}
