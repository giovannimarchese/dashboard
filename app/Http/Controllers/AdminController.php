<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Server;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * admin homepage
     * @return view
     */
    public function index(){

        return view('pages/adminHome');
    }

    /**
     * New user form
     * @return view
     */
    public function newUser(){

        return view('auth/register');
    }

    /**
     * Create a new user from post
     * After create a notification for them
     * @param RegistrationRequest $request
     * @return redirect (back to admin homepage with flash message)
     */
    public function registerUser(RegistrationRequest $request){

        $id = $this->createUser($request->all())->id;

        $this->makeNotification($id);

        $name = $request->name . ' ' . $request->lastname;

        return redirect('/admin')->with('flash_message', 'The user for '.$name.' was added!');
    }

    /**
     * Show all users table
     * @return view
     */
    public function allUsers(){

        $users = User::all();

        return view('pages/allUsers', compact('users'));
    }

    /**
     * Edit user form
     * @param $id
     * @return view
     */
    public function editUser($id){

        $user = User::find($id);

        return view('pages/editUser', compact('user'));
    }

    /**
     * @param UpdateUserRequest $request
     * @return redirect (back to users table with flash message)
     */
    public function updateUser(UpdateUserRequest $request){

        $this->update($request->all());

        $name = $request->name . ' ' . $request->lastname;

        return redirect('/admin/users')->with('flash_message', $name.' was updated!');
    }

    /**
     * Delete users after delete linked notification(s)
     * @param $id
     */
    public function deleteUser($id){

        User::destroy($id);
        Notification::where('user_id', $id)->delete();
    }

    /**
     * Create new user
     * @param $data
     * @return static
     */
    private function createUser($data){

        if(isset($data['admin'])) {
            if($data['admin'] == 'on') $admin = 1;
        } else{
            $admin = 0;
        }

        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'admin' => $admin
        ]);
    }

    /**
     * Create new notification for user
     * @param $id
     */
    private function makeNotification($id){

        Notification::create(['user_id' => $id]);
    }

    /**
     * Update user
     * @param $data
     */
    private function update($data){

        if(isset($data['admin'])) {
            if($data['admin'] == 'on') $data['admin'] = 1;
        } else{
            $data['admin'] = 0;
        }

        if($data['password'] == '') {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }

        User::find($data['id'])->update($data);

    }

    /**
     * Show table with all servers
     * @return view
     */
    public function newServer(){

        $servers = Server::all();

        return view('pages/server', compact('servers'));
    }

    /**
     * Add new server from post
     * @param Request $request
     * @return redirect (back to server table with flash message)
     */
    public function addServer(Request $request){

        Server::create($request->input());

        return redirect('/admin/server')->with('flash_message', 'server ' .$request->input('name'). ' was added!');
    }
}
