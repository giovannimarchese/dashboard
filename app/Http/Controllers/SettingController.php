<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Load settings overview
     * @return view
     */
    public function index(){

        $settings = Setting::all();

        return view('pages/settings', compact('settings'));
    }

    /**
     * Update settings
     * @param Request $request
     * @return redirect (back to settings overview with flash message)
     */
    public function update(Request $request){

        $settings = $request->input();
        unset($settings['_token']);

        foreach($settings as $key => $setting){
            Setting::where('name', $key)->update($setting);
        }

        return redirect('/admin/settings')->with('flash_message', 'The settings were updated');
    }
}
