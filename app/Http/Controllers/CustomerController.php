<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Load customers table with data from cache
     * @return view
     */
    public function index(){

        $customers = Cache::get('customers');

        return view('pages/customers', compact('customers'));
    }


    /**
     * get data for linechart about the amount of mailings for a customer
     * by choosing a client and a number of months we find the amount
     * of mailings per month for a client to generate a linechart from
     * @param Request $request
     * @return array
     */
    public function mailings(Request $request){

        $id = $request->input('id');
        $months = $request->input('months');

        $mailings = DB::table('mailing')
            ->select(
                DB::raw('COUNT(*) as mailings,
                        MONTH(starttime) `month`,
                        YEAR(starttime) as `year`')
            )
            ->where(
                DB::raw('DATE(starttime)'), '>', DB::raw('DATE(DATE_SUB(NOW(), INTERVAL '.$months.' MONTH))')
            )
            ->where('customer_id', $id)
            ->where('status', 'completed')
            ->groupBy(DB::raw('MONTH(starttime)'))
            ->groupBy(DB::raw('YEAR(starttime)'))
            ->orderBy('year')
            ->orderBy('month')
            ->get();

        $chart = [];
        foreach($mailings as $mailing){
            $formatDate = date_format(\DateTime::createFromFormat('Y-n', $mailing->year.'-'.$mailing->month), 'Y-m');
            array_push($chart, [$formatDate, $mailing->mailings]);
        }

        return $chart;
    }
}