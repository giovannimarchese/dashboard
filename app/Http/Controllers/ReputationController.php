<?php

namespace App\Http\Controllers;

use App\Score;
use App\Server;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReputationController extends Controller
{
    /**
     * Get all reputation scores
     * For reputation tables
     * @return view
     */
    public function index(){

        $senderscore = $this->getSenderscore();
        $senderbase = $this->getSenderbase();
        $snds = $this->getSNDS();

        return view('pages/reputation', compact('senderscore', 'senderbase', 'snds'));
    }

    /**
     * Get data for reputation chart
     * We do some special loops so charts
     * Don't break when you add a new server
     * @param $days
     * @return array
     */
    public function getGraph($days){

        $date = Carbon::now()->subDays($days);

        $scores = Score::where('ts', '>=', $date)->get();
        $dates = [];
        foreach($scores as $score){
            $date = substr($score->ts, 0, 10);
            if(!isset($dates[$date])){
                $dates += [$date => $date];
            }
        }

        $charts['servers'] = Server::all();
        $charts['scores'] = [];
        $this->result = 0;
        foreach($charts['servers'] as $server){
            $i = 0;
            foreach($dates as $date){
                foreach($scores as $key => $score){
                    if($score->server_id == $server->id && substr($score->ts, 0, 10) == $date){
                        $this->result = $score->score;
                        unset($scores[$key]);
                        break;
                    }else{
                        $this->result = false;
                    }
                }
                if(!isset($charts['scores'][$i])){
                    $charts['scores'][$i] = array($date);
                }
                if($this->result){
                    if($this->result == '?'){
                        $charts['scores'][$i][] = null;
                    }else{
                        $charts['scores'][$i][] = (int) $this->result;
                    }
                }else{
                    $charts['scores'][$i][] = null;
                }
                $i++;
            }
        }
        return $charts;
    }

    /**
     * Get all reputation data
     * for refilling all reputation tables
     * with chosen date
     * @param $date
     * @return mixed
     */
    public function getReputation($date){

        $reputation['senderscore'] = $this->getSenderscore($date);
        $reputation['senderbase'] = $this->getSenderbase($date);
        $reputation['snds'] = $this->getSNDS($date);

        return $reputation;
    }

    /**
     * Get senderscores
     * @param null $date
     * @return array
     */
    private function getSenderscore($date = null)
    {
        $senderscore = Server::with(['scores' => function($query) use (&$date){
            if($date){
                $query->where('ts', 'like', $date.'%')->orderBy('ts', 'desc');
            }else{
                $query->orderBy('ts', 'desc')->take(Server::count());
            }
        }])->get();

        return $senderscore;
    }

    /**
     * Get senderbase
     * @param null $date
     * @return array
     */
    private function getSenderbase($date = null)
    {
        $senderbase = Server::with(['bases' => function($query) use (&$date){
            if($date){
                $query->where('ts', 'like', $date.'%')->orderBy('ts', 'desc');
            }else{
                $query->orderBy('ts', 'desc')->take(Server::count());;
            }
        }])->get();

        return $senderbase;
    }

    /**
     * Get SNDS
     * @param null $date
     * @return array
     */
    private function getSNDS($date = null)
    {
        $snds = Server::with(['snds' => function($query) use (&$date){
            if($date){
                $query->where('till', 'like', $date.'%')->orderBy('till', 'desc');
            }else{
                $query->orderBy('till', 'desc')->take(Server::count());;
            }
        }])->get();

        return $snds;
    }
}