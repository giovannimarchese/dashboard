<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Load user page
     * @return view
     */
    public function index(){

        $user = Auth::user();

        return view('pages/account', compact('user'));
    }

    /**
     * Update user profile
     * @param ProfileRequest $request
     * @return redirect (back to user page with flash message)
     */
    public function update(ProfileRequest $request){
        
        $user = Auth::user();
        $user['name'] = $request->name;
        $user['lastname'] = $request->lastname;
        $user['email'] = $request->email;

        if (!empty($request->password_old)) {
            if (Hash::check($request->password_old, $user['password'])) {
                if ($request->password)
                    $user['password'] = bcrypt($request->password);
            } else {
                return redirect('account')->with('flash_message', 'Your old password was incorrect!');
            }
        } else if(empty($request->password_old) && !empty($request->password)){
            return redirect('account')->with('flash_message', 'Your old password was empty!');
        }

        if ($request->hasFile('image')) {
            $imageName = $request->name . $request->lastname . Carbon::now()->timestamp . '.' . $request->file('image')->guessExtension();
            $request->file('image')->move(base_path() . '/public/img/profile/', $imageName);
            $user['image'] = $imageName;
        }

        $user->save();

        return redirect('account')->with('flash_message', 'Your account was updated!');
    }
}
