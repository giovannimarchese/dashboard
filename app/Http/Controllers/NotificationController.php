<?php

namespace App\Http\Controllers;

use App\Flagged;
use App\Notification;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    /**
     * Check if we need to display a notification
     * If so call function to make it and return an array
     * @return array|string
     */
    public function checkForNotification(){

        $user_id = Auth::user()->id;

        $now = Carbon::now()->subHours(24);

        $lastFlag = Notification::where('user_id', $user_id)->where('type', 'flagged')->first()->last;

        $flags = Flagged::latest()->where('created_at', '>', $now)->where('id', '>', $lastFlag)->get();

        if($flags->count()){
            return $this->makeNotification($flags, $user_id);
        }else{
            return json_encode('');
        }

    }

    /**
     * Make the notification with giving params
     * @param $flags
     * @param $user_id
     * @return array
     */
    private function makeNotification($flags, $user_id){

        if($flags->count() == 1){
            $notification['body'] = $flags[0]->message;

            if($flags[0]->type == 'reputation'){
                if($flags[0]->foreign_id == 0){
                    $notification['title'] = 'All servers are doing okay!';
                }else{
                    $notification['title'] = $flags[0]->server->name;
                }
                $notification['url'] = url().'/reputation';
            }else if($flags[0]->type == 'mailing'){
                $notification['title'] = $flags[0]->mailing->name;
                $notification['url'] = url().'/mailings/MC'.$flags[0]->foreign_id;
            }else if($flags[0]->type == 'note'){
                $notification['title'] = $flags[0]->user->name.' '.$flags[0]->user->lastname.':';
                $notification['url'] = url().'/flagged';
            }else if($flags[0]->type == 'update'){
                $notification['title'] = 'Dashboard Update:';
                $notification['url'] = url().'/flagged';
            }
        }else{
            $notification['title'] = 'Several new flags!';
            $notification['body'] = 'There are '.$flags->count().' flags you haven\'t seen yet';
            $notification['url'] = url().'/flagged';
        }

        Notification::where('user_id', $user_id)->where('type', 'flagged')->update(['last' => $flags[0]->id]);

        return $notification;
    }

    /**
     * Notification settings
     * @param Request $request
     * @return string
     */
    public function changeNotification(Request $request){

        $update = [
            'user_id' => $request->user_id,
            'type' => $request->type,
            'notification' => $request->notification,
            'use' => $request->use,
            'last' => Flagged::latest()->first()->id
        ];

        Notification::where('user_id', $request->user_id)->update($update);

        return json_encode('saved');

    }
}
