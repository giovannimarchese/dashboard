<?php

namespace App\Http\Controllers;

use App\Traffic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrafficController extends Controller
{

    /**
     * Load traffic page
     * @return view
     */
    public function index(){

        return view('pages/traffic');
    }

    /**
     * Get the traffic for today
     * So we can populate a linechart
     * @param Request $request
     * @return array
     */
    public function getLineChartForDate(Request $request)
    {
        $date = $request->input('date');

        $traffic = Traffic::where('date', '=', $date)->get();

        $total = [];
        $delivered = [];
        $deferred = [];
        $bounced = [];
        $rejected = [];

        foreach ($traffic as $traf) {

            if (!isset($total[$traf->ts])) {
                $temp = ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);

                $total[$traf->ts] = $temp;
                $delivered[$traf->ts] = $traf->delivered;
                $deferred[$traf->ts] = $traf->deferred;
                $bounced[$traf->ts] = $traf->bounced;
                $rejected[$traf->ts] = $traf->rejected;
            } else {
                $temp = ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);

                $total[$traf->ts] += $temp;
                $delivered[$traf->ts] += $traf->delivered;
                $deferred[$traf->ts] += $traf->deferred;
                $bounced[$traf->ts] += $traf->bounced;
                $rejected[$traf->ts] += $traf->rejected;
            }
        }
        $traffic = array(
            'total' => $total,
            'delivered' => $delivered,
            'deferred' => $deferred,
            'bounced' => $bounced,
            'rejected' => $rejected
        );

        return $traffic;
    }

    /**
     * Get the traffic for today
     * So we can populate the pie charts
     * @param Request $request
     * @return array
     */
    public function getPieChartForDate(Request $request){

        $date = $request->input('date');

        $traffic = Traffic::where('date', '=', $date)->get();

        $delivered = [];
        $server = [];

        foreach($traffic as $traf){

            if(!isset($delivered['total'])) {
                $temp = ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);

                $delivered['total'] = $temp;
                $delivered['delivered'] = $traf->delivered;
                $delivered['deferred'] = $traf->deferred;
                $delivered['bounced'] = $traf->bounced;
                $delivered['rejected'] = $traf->rejected;
            }else{
                $temp = ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);

                $delivered['total'] += $temp;
                $delivered['delivered'] += $traf->delivered;
                $delivered['deferred'] += $traf->deferred;
                $delivered['bounced'] += $traf->bounced;
                $delivered['rejected'] += $traf->rejected;
            }

            if(!isset($server[$traf->server])){
                $server[$traf->server] = ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);
            }else{
                $server[$traf->server] += ($traf->delivered + $traf->deferred + $traf->bounced + $traf->rejected);
            }
        }

        $traffic = array(
            'delivered' => $delivered,
            'server' => $server
        );

        return $traffic;
    }
}
