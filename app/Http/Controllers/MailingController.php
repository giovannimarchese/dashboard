<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerDatabase;
use App\Hardbounce;
use App\Http\Helper;
use App\Mailing;
use App\Spam;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailingController extends Controller
{
    /**
     * Load mailing page with this month of given month
     * @param null $month
     * @return redirect (back with flash message)| view
     */
    public function index($month = null){

        if($month != null) {
            if(substr($month, 0, 2) == 'MC'){
                $mailing_id = substr($month, 2);
                if($mailing_id != '')
                    $mailing = Mailing::find($mailing_id);

                $month = null;
            }else{
                $monthTitle = date_format(\DateTime::createFromFormat('Y-m', $month), 'F - Y');
                if (!\DateTime::createFromFormat('Y-m', $month) || strlen($month) != 7)
                    return back()->with('flash_message', 'This is not a valid date');
            }
        }

        $customers = Customer::all();

        $page = "index";

        return view('pages/mailing', compact('customers', 'month', 'monthTitle', 'page', 'mailing'));
    }

    /**
     * Load mailing page for specific date
     * @param null $date
     * @return redirect (back with flash message)| view
     */
    public function mailingsForDate($date = null){

        if(!\DateTime::createFromFormat('Y-m-d', $date))
            return back()->with('flash_message', 'This is not a valid date');

        $customers = Customer::all();

        $page = 'date';

        return view('pages/mailing', compact('customers', 'date', 'page'));
    }

    /**
     * Load mailing page for specific customer or database
     * Optionally also for a specific date
     * @param null $date
     * @return redirect (back with flash message)| view
     */
    public function mailingsForCustomer($customer_id, $date = null){

        $id_prefix = substr($customer_id, 0, 1);
        $id = substr($customer_id, 1);

        if($id_prefix == 'C'){
            $customer = Customer::find($id);
        }else if($id_prefix == 'D'){
            $customer = CustomerDatabase::find($id);
        }else{
            return back()->with('flash_message', 'Customer/Database ID is not in the right format');
        }

        if(empty($customer))
            return back()->with('flash_message', 'We couldn\'t find a Customer/Database with this ID');

        $customers = Customer::all();

        $page = "customer";

        return view('pages/mailing', compact('customers', 'customer', 'customer_id', 'date', 'page'));
    }

    /**
     * Get data for jquery dataTable
     * This function only get data for a specific month
     * @param null $month
     * @return array
     */
    public function table($month = null){

        if($month){
            $mailingsData = Mailing::selectMailing()->with('database')->where('starttime', 'like', $month.'%')->latest()->completed()->get();
        }else{
            $mailingsData = Mailing::selectMailing()->with('database')->thisMonth()->latest()->completed()->get();
        }

        $mailings = $this->formatMails($mailingsData);

        return ["data" => $mailings];
    }

    /**
     * Get data for jquery dataTable
     * This function only get data for a specific date
     * @param null $date
     * @return array
     */
    public function dateTable($date = null){

        $mailingsData = Mailing::selectMailing()->with('database')->latest()->completed()->where('starttime', 'like', $date.'%')->get();

        if(empty($mailingsData->toArray()))
            return ["date" => []];

        $mailings = $this->formatMails($mailingsData);

        return ["data" => $mailings];
    }

    /**
     * Get data for jquery dataTable
     * This function only get data for a
     * Customer or a Database
     * Optionally also for a specific date
     * @param $customer
     * @param null $date
     * @return array
     */
    public function customerTable($customer, $date = null) {

        $id_prefix = substr($customer, 0, 1);
        $id = substr($customer, 1);

        if($id_prefix == 'C'){
            $mailingsData = Mailing::selectMailing()->with('database')->latest()->completed()->where('customer_id', $id);
        }else if($id_prefix == 'D'){
            $mailingsData = Mailing::selectMailing()->with('database')->latest()->completed()->where('customer_database_id', $id);
        }else{
            return ["data" => []];
        }

        if(!empty($date))
            $mailingsData = $mailingsData->where('starttime', 'like', $date.'%');

        $mailingsData = $mailingsData->get();

        if(empty($mailingsData->toArray()))
            return ["data" => []];

        $mailings = $this->formatMails($mailingsData);

        return ["data" => $mailings];
    }

    /**
     * Get all databases for selectbox
     * @return array
     */
    public function getDatabases(Request $request){

        $databases = Customer::find($request->input('id'))->databases()->get()->toArray();

        return $databases;
    }

    /**
     * Get data for a specific mailing
     * with this we generate a barchart
     * @return array
     */
    public function getBarData(Request $request){

        $mailing = Mailing::find($request->input('id'));

        if($mailing){
            $data['total'] = $mailing->num_mails;
            $data['reads'] = $mailing->reads_unique;
            $data['clicks'] = $mailing->clicks_unique;
            $data['conversions'] = $mailing->conversions_unique;
            $data['hb'] = $mailing->hardbounces;
            $data['sb'] = $mailing->softbounces;
            $data['spam'] = $mailing->spamcomplaints;
        }else{
            $data = json_encode('notfound');
        }

        return $data;
    }

    /**
     * Formats the data of the table functions
     * To prepare it for the view
     * @param $mailingsData
     * @return array
     */
    private function formatMails($mailingsData){

        $count = 0;
        foreach ($mailingsData as $mailing) {

            $mailings[$count] = array(
                'id' => $mailing->id,
                'customer_db' => $mailing->database->name,
                'db_id' => $mailing->customer_database_id,
                'old_mail_id' => $mailing->old_mailing_id,
                'name' => $mailing->name,
                'mails' => $mailing->num_mails,
                'reads' => Helper::calculatePercentage($mailing->num_mails, $mailing->reads_unique),
                'clicks' => Helper::calculatePercentage($mailing->num_mails, $mailing->clicks_unique),
                'conversions' => Helper::calculatePercentage($mailing->num_mails, $mailing->conversions_unique),
                'hb' => Helper::calculatePercentage($mailing->num_mails, $mailing->hardbounces),
                'sb' => Helper::calculatePercentage($mailing->num_mails, $mailing->softbounces),
                'spam' => Helper::calculatePercentage($mailing->num_mails, $mailing->spamcomplaints),
                'timestamp' => $mailing->starttime,
            );
            $count++;
        }
        if(!isset($mailings)) return [];

        return $mailings;
    }


    /**
     * Show list of complainers (emails)
     * @param $mailing_id
     * @param $database_id
     * @return array
     */
    public function spam($mailing_id, $database_id){

        $spam['complainers'] = Spam::where('mailing_id', $mailing_id)->where('customer_database_id', $database_id)->get();

        $spam['chart'] = [];
        foreach($spam['complainers'] as $complainer){
            $pos = strpos($complainer->subscriber, '@');
            $domain = substr($complainer->subscriber, ($pos + 1));
            if(isset($spam['chart'][$domain])){
                $spam['chart'][$domain]++;
            }else{
                $spam['chart'][$domain] = 1;
            }
        }

        return $spam;
    }

    /**
     * Get all hardbounces
     * Make arrays that only save stuff we need
     * Now we loop over them and when we see a duplicate,
     * We don't save it we just ++ on the amount.
     * Then we sort them up highest amount first
     * @param $mailing_id
     * @param $database_id
     * @return array
     */
    public function hardbounce($mailing_id, $database_id){

        $bounces = Hardbounce::where('mailing_id', '=', $mailing_id)->where('customer_database_id', '=', $database_id)->get();

        $hardbounces = [];
        foreach($bounces as $bounce){
            $hardbounces[] = [
                'domain' => $bounce->subscriber,
                'code' => $bounce->smtp_code,
                'status' => $bounce->smtp_status,
                'amount' => 1
            ];
        }

        $merged = [];
        foreach($hardbounces as &$hardbounce){
            $pos = strpos($hardbounce['domain'], '@');
            $hardbounce['domain'] = substr($hardbounce['domain'], ($pos + 1));
            if(empty($merged)){
                $merged[] = $hardbounce;
            }else{
                $i = 0;
                foreach($merged as $key => $merge){
                    $i++;
                    if($hardbounce['domain'] == $merge['domain'] && $hardbounce['code'] == $merge['code'] && $hardbounce['status'] == $merge['status']){
                        $merged[$key]['amount']++;
                        break;
                    }
                    if(count($merged) == $i){
                        $merged[] = $hardbounce;
                    }
                }
            }
        }

        usort($merged, function($a, $b){
            return $b['amount'] - $a['amount'];
        });

        return $merged;
    }


}
