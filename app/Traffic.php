<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hour_traffic';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Scope to get oldest results first
     * @param $query
     * @return mixed
     */
    public function scopeOldest($query){

        return $query->orderBy('date', 'asc');
    }
}
