<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Flagged extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'flagged';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the user that owns the flag.
     */
    public function user(){

        return $this->belongsTo('App\User');
    }

    /**
     * Get the server that belongs to this flag
     */
    public function server(){

        return $this->hasOne('App\Server', 'id', 'foreign_id');
    }

    /**
     * Get the mailing that belongs to this flag
     */
    public function mailing(){

        return $this->hasOne('App\Mailing', 'id', 'foreign_id');
    }

    /**
     * Scope to get latest results first
     * @param $query
     * @return mixed
     */
    public function scopeLatest($query){

        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Modify created_at column
     * To display it as X time ago
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value){

        return Carbon::parse($value)->diffForHumans();
    }
}
