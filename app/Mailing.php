<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mailing';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the db that owns the mailing.
     */
    public function database(){

        return $this->belongsTo('App\CustomerDatabase', 'customer_database_id');
    }

    /**
     * Scope to get latest results first
     * @param $query
     * @return mixed
     */
    public function scopeLatest($query){

        return $query->orderBy('starttime', 'desc');
    }

    /**
     * Scope to get oldest results first
     * @param $query
     * @return mixed
     */
    public function scopeOldest($query){

        return $query->orderBy('starttime', 'asc');
    }

    /**
     * Scope to get only results that are completed
     * @param $query
     * @return mixed
     */
    public function scopeCompleted($query){

        return $query->where('status', '=', 'completed');
    }

    /**
     * Scope to get only results that are not completed
     * @param $query
     * @return mixed
     */
    public function scopeNotCompleted($query){

        return $query->where('status', 'setup')->orWhere('status', 'pending')->orWhere('status', 'running');
    }

    /**
     * Scope to get only results from this month
     * @param $query
     * @return mixed
     */
    public function scopeThisMonth($query){

        return $query->where('ts', 'like', date('Y-m').'%');
    }

    /**
     * Scope for a very long select
     * @param $query
     * @return mixed
     */
    public function scopeSelectMailing($query){

        return $query->select('id', 'customer_db', 'customer_database_id', 'old_mailing_id', 'name', 'num_mails', 'reads_unique', 'clicks_unique', 'conversions_unique', 'hardbounces', 'softbounces', 'spamcomplaints', 'starttime');
    }

    /**
     * Scope to get mailings
     * With minimum amount of mails
     * @param $query
     * @param $min
     * @return mixed
     */
    public function scopeMails($query, $min){

        return $query->where('num_mails', '>=', $min);
    }
}
