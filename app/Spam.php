<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spamcomplaint';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
