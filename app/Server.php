<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'server';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip', 'name', 'location', 'type'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get scores for server
     */
    public function scores(){

        return $this->hasMany('App\Score');
    }

    /**
     * Get senderbase for server
     */
    public function bases(){

        return $this->hasMany('App\Base');
    }

    /**
     * Get snds for server
     */
    public function snds(){

        return $this->hasMany('App\Sndsscore');
    }
}
