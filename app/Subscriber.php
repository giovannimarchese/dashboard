<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscriber_archive';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get hardbounce for this subscriber.
     */
    public function hardbounce(){

        return $this->hasMany('App\Hardbounce');
    }

    /**
     * Get spamcomplaints for this subscriber.
     */
    public function spam(){

        return $this->hasMany('App\Spam');
    }

}
