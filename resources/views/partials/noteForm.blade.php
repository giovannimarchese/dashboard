<div id="mailForm">
    <p class="note-head">Add a note</p>

    <form action="/flagged" method="post" id="flagForm">
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        @if(!Auth::user()->developer)
        <input type="hidden" name="type" value="note">
        @endif
        <div class="row">
            <div class="input-field col s6">
                <select name="alert" class="flagSelect" data-required>
                    <option value="" disabled selected>Alert level</option>
                    <option value="red">Red</option>
                    <option value="orange">Orange</option>
                    <option value="green">Green</option>
                </select>
            </div>
            @if(Auth::user()->developer)
                <div class="input-field col s6">
                    <select name="type" class="flagSelect" data-required>
                        <option value="note" selected>Note</option>
                        <option value="update">Dashboard Update</option>
                    </select>
                </div>
            @endif
            <div class="input-field col s12">
                <label for="message">Message/Comment</label>
                <textarea id="message" name="message" class="materialize-textarea" length="120" @if(!Auth::user()->developer)maxlength="140"@endif data-required></textarea>
            </div>
        </div>
        <button type="submit" id="postFlag" class="waves-effect waves-light btn cyan lighten-2"><i class="material-icons right">flag</i>Flag Note</button>
    </form>

</div>
