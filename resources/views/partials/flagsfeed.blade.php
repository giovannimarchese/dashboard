@foreach($flags as $flag)
    <div class="card-panel hoverable flag-panel cf">
        <span class="time"><i class="material-icons right">flag</i>{{$flag->created_at}}</span>
        @if($flag->type == 'reputation')
            <a href="/reputation">
                <i class="material-icons left text-{{$flag->alert}}">storage</i>
                @if($flag->foreign_id)
                    <p class="flagHead">{{$flag->server->name}}</p>
                @else
                    <p class="flagHead">Overall server reputation</p>
                @endif
            </a>
        @elseif($flag->type == 'mailing')
            <a href="/mailings/MC{{$flag->foreign_id}}">
                <i class="material-icons left text-{{$flag->alert}}">mail</i>
                <p class="flagHead">{{$flag->mailing->name}} - {{$flag->mailing->customer_db}}</p>
            </a>
        @elseif($flag->type == 'note')
            <i class="material-icons left text-{{$flag->alert}}">announcement</i>
            <p class="flagHead">Note</p>
        @elseif($flag->type == 'update')
            <i class="material-icons left text-{{$flag->alert}}">update</i>
            <p class="flagHead">Dashboard Update</p>
        @endif
        @if($flag->user)
            @if($flag->user->image)
                <img src="/img/profile/{{$flag->user->image}}" class="image">
            @endif
            <div class="content">
                <p class="author">{{$flag->user->name}} {{$flag->user->lastname}}:</p>
        @else
            <div class="content">
        @endif
            <p class="body">{{$flag->message}}</p>
        </div>
    </div>
@endforeach