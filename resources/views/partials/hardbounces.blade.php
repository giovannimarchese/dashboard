<div id="hardbounceModal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4><span class="hbcount"></span> Hardbounces</h4>
        <div class="row">
            <div class="col s12">
                <p class="nohb hide">We couldn't find any hardbounces</p>
                <table class="hbtable striped highlight">
                    <thead>
                        <tr>
                            <th data-field="domain">Domain</th>
                            <th data-field="code">SMTP code</th>
                            <th data-field="status">SMTP status</th>
                            <th data-field="amount">Amount</th>
                        </tr>
                    </thead>

                    <tbody class="hardbounces">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
