<div id="serverForm" class="modal bottom-sheet">
    <form action="/admin/server" method="post" id="serverForm">
        <div class="modal-content">
            <h4>New server</h4>

            <div class="row">
                <div class="input-field col s6">
                    <select name="type" class="serverSelect" data-required>
                        <option value="" disabled selected>Type</option>
                        <option value="medium quality">Medium Quality</option>
                        <option value="high quality">High Quality</option>
                    </select>
                </div>

                <div class="input-field col s6">
                    <label for="ip">ip address</label>
                    <input type="text" name="ip" id="ip">
                </div>

                <div class="input-field col s6">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name">
                </div>

                <div class="input-field col s6">
                    <label for="location">Location</label>
                    <input type="text" name="location" id="location">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" id="addServer" class="waves-effect waves-light btn-large cyan lighten-2"><i class="material-icons right">storage</i>Add server</button>
        </div>
        {!! csrf_field() !!}
    </form>
</div>
