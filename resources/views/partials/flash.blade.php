@if(Session::has('flash_message'))
    <script>
        $(function(){
            Materialize.toast('{{ Session::get('flash_message') }}', 2500);
        });
    </script>
@endif

@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
            $(function(){
                Materialize.toast('{{ $error}}', 2500);
            });
        </script>
    @endforeach
@endif