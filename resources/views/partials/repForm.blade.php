<div id="repForm" class="modal bottom-sheet">
    <form action="/flagged" method="post" id="flagForm">
        <div class="modal-content">
            <h4>Flag reputation</h4>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="type" value="reputation">
            <div class="row">
                <div class="input-field col s6">
                    <select name="foreign_id" class="flagSelect" data-required>
                        <option value="" disabled selected>Choose server</option>
                        <option value="0">None specific</option>
                        @foreach($servers as $server)
                        <option value="{{$server->id}}">{{$server->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-field col s6">
                    <select name="alert" class="flagSelect" data-required>
                        <option value="" disabled selected>Alert level</option>
                        <option value="red">Red</option>
                        <option value="orange">Orange</option>
                        <option value="green">Green</option>
                    </select>
                </div>
                <div class="input-field col s12">
                    <label for="message">Message/Comment</label>
                    <textarea id="message" name="message" class="materialize-textarea" length="120" maxlength="140" data-required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" id="postFlag" class="waves-effect waves-light btn-large cyan lighten-2"><i class="material-icons right">flag</i>Flag It</button>
        </div>
    </form>
</div>
