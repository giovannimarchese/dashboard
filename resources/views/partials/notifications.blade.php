<div id="changeNotifications" class="modal">
    <form action="/notifications" method="post" id="notificationsForm">
        <div class="modal-content">
            <h4>Your Notifications Settings</h4>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="type" value="flagged">

            <div class="row">
                <div class="col s6">

                    <p>How do you want to receive your notifications?</p>
                    <select name="notification" class="notificationSelect">
                        <option value="desktop" @if($notification->notification == 'desktop') selected @endif>Desktop</option>
                        <option value="mail" @if($notification->notification == 'mail') selected @endif>Mail</option>
                        <option value="both" @if($notification->notification == 'both') selected @endif>Both</option>
                    </select>
                </div>
                <div class="col s6">

                    <p>Do you want to be notified at all?</p>
                    <div class="switch">
                        <label>
                            No
                            <input type="hidden" name="use" value="0">
                            <input type="checkbox" name="use" value="1" @if($notification->use) checked @endif >
                            <span class="lever"></span>
                            Yes
                        </label>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="waves-effect waves-light btn-large cyan lighten-2">Update</button>
        </div>
    </form>
</div>
