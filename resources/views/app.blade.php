<!DOCTYPE html>

<html>

<head>
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/css/app.css">
    @yield('styles')
</head>

<body>

    @include('header')

    <main>
        @yield('content')
    </main>

    @yield('footer')

    <script type="text/javascript" src="/js/lib/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/lib/materialize.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    @if($notification)
        <script type="text/javascript" src="/js/notification.js"></script>
    @endif
    @yield('scripts')


    @include('partials/flash')

</body>

</html>