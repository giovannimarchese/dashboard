@if(Auth::check())
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper">
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="/home" class="home-button brand-logo hide-on-large-only"></a>
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <li><a href="/home" class="home-button"></a></li>
                <li><a href="/reputation">Reputation</a></li>
                <li><a href="/traffic">Traffic</a></li>
                <li><a href="/volume">Volume</a></li>
                <li><a href="/customers">Customers</a></li>
                <li><a href="/mailings">Mailings</a></li>
                <li><a href="/domains">Domains</a></li>
                <li><a href="/flagged">Flagged</a></li>
                @if(Auth::user()->admin) <li><a href="/admin">Admin</a></li> @endif
            </ul>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li class="noBorder"><a class="dropdown-button iAnchor" href="#!" data-activates="dropdown1"><span id="initials">{{$initials}}</span></a></li>
            </ul>
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="/account">Account</a></li>
                <li><a href="/logout">Logout</a></li>
            </ul>

            {{--mobile nav--}}
            <ul class="side-nav" id="mobile-demo">
                <li><a href="/home">Home</a></li>
                <li><a href="/reputation">Reputation</a></li>
                <li><a href="/traffic">Traffic</a></li>
                <li><a href="/volume">Volume</a></li>
                <li><a href="/customers">Customers</a></li>
                <li><a href="/mailings">Mailings</a></li>
                <li><a href="/domains">Domains</a></li>
                <li><a href="/flagged">Flagged</a></li>
                @if(Auth::user()->admin) <li><a href="/admin">Admin</a></li> @endif
                <li><a href="/account">Account</a></li>
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </nav>
</div>
@else
<div class="topbar">
    <img src="/img/logo.png">
</div>
@endif