@extends('app')

@section('content')

    <div class="row">
        <div class="col l8 offset-l2 m12 s12">
            <div class="settingsWrapper cf">
                <form method="post" action="/admin/settings" id="settingsForm">
                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>Flag reputation red</b>
                            <p class="textWrap">
                                Flag server red when score is equal or below <input type="number" name="red_server[var]" value="{{$settings[1]->var}}">
                            <div class="input-field">
                                <label for="red_flag_msg">Red flag message:</label>
                                <input id="red_flag_msg" name="red_server[body]" type="text" value="{{$settings[1]->body}}">
                            </div>
                            </p>
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="hidden" name="red_server[use]" value="0">
                                    <input type="checkbox" name="red_server[use]" value="1" @if($settings[1]->use) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>Flag reputation orange</b>
                            <p class="textWrap">
                                Flag server orange when score is equal or below <input type="number" name="orange_server[var]" value="{{$settings[2]->var}}">
                            <div class="input-field">
                                <label for="orange_flag_msg">Orange flag message:</label>
                                <input id="orange_flag_msg" name="orange_server[body]" type="text" value="{{$settings[2]->body}}">
                            </div>
                            </p>
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="hidden" name="orange_server[use]" value="0">
                                    <input type="checkbox" name="orange_server[use]" value="1" @if($settings[2]->use) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>Flag reputation green</b>
                            <p class="textWrap">
                                When all servers are equal or above <input type="number" name="green_server[var]" value="{{$settings[3]->var}}"> set a flag with their average
                            <div class="input-field">
                                <label for="green_flag_msg">Green flag message:</label>
                                <input id="green_flag_msg" name="green_server[body]" type="text" value="{{$settings[3]->body}}">
                            </div>
                            </p>
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="hidden" name="green_server[use]" value="0">
                                    <input type="checkbox" name="green_server[use]" value="1" @if($settings[3]->use) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>Flag mailing red</b>
                            <p class="textWrap">
                                Flag mailing red when delivery rate is under <input type="number" name="red_mailing[var]" value="{{$settings[4]->var}}">
                            <div class="input-field">
                                <label for="red_mail_msg">Red flag message:</label>
                                <input id="red_mail_msg" name="red_mailing[body]" type="text" value="{{$settings[4]->body}}">
                            </div>
                            </p>
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="hidden" name="red_mailing[use]" value="0">
                                    <input type="checkbox" name="red_mailing[use]" value="1" @if($settings[4]->use) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>Flag mailing orange</b>
                            <p class="textWrap">
                                Flag mailing orange when delivery rate is under <input type="number" name="orange_mailing[var]" value="{{$settings[5]->var}}">
                            <div class="input-field">
                                <label for="orange_mail_msg">Orange flag message:</label>
                                <input id="orange_mail_msg" name="orange_mailing[body]" type="text" value="{{$settings[5]->body}}">
                            </div>
                            </p>
                            <div class="switch">
                                <label>
                                    Off
                                    <input type="hidden" name="orange_mailing[use]" value="0">
                                    <input type="checkbox" name="orange_mailing[use]" value="1" @if($settings[5]->use) checked @endif >
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col m6 s12">
                        <div class="card-panel hoverable block">
                            <b>More variables</b>
                            <p class="textWrap">
                                Update imported mailings for <input type="number" name="update_days[var]" value="{{$settings[0]->var}}"> days
                            </p>
                            <p class="textWrap">
                                In order to get flagged a mailing must have at least <input type="number" name="min_mails[var]" value="{{$settings[6]->var}}" style="width:50px;"> mails
                            </p>
                            <p class="textWrap">
                                Update mailing flags for <input type="number" name="update_hours[var]" value="{{$settings[7]->var}}"> hours
                            </p>
                        </div>
                    </div>

                    <div class="col s12">
                        <button class="waves-effect waves-light btn cyan lighten-2" type="submit"><i class="material-icons right">settings</i>Update settings</button>
                    </div>
                {!! csrf_field() !!}
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')

@stop