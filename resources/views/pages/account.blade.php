@extends('app')

@section('content')

    <div class="row">
        <div class="col l4 m6 s12 offset-l4 offset-m3">
            <div class="card-panel hoverable cf">

                <form method="POST" action="/account/{{ $user->id }}" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <h5 class="center">My Account</h5>
                    <div class="col s4 offset-s4">
                        <span class="profile_img">
                            @if(!empty(Auth::user()->image))
                                <img src="img/profile/{{Auth::user()->image}}" width="100%" height="100%">
                            @else
                                {{$initials}}
                            @endif
                        </span>
                    </div>
                    <div class="input-field col s6">
                        <label for="firstname">Firstname</label>
                        <input type="text" name="name" value="{{ $user->name }}" id="firstname">
                    </div>

                    <div class="input-field col s6">
                        <label for="lastname">Lastname</label>
                        <input type="text" name="lastname" value="{{ $user->lastname }}" id="lastname">
                    </div>

                    <div class="input-field col s12">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{ $user->email }}" id="email">
                    </div>

                    <div class="file-field input-field col s12">
                        <div class="btn cyan-lighten">
                            <span>Choose</span>
                            <input type="file" name="image" id="profile_input">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="A Profile Picture *">
                        </div>
                    </div>

                    <div class="input-field col s12">
                        <label for="password_old">Old Password *</label>
                        <input type="password" name="password_old" id="password_old">
                    </div>

                    <div class="input-field col s6">
                        <label for="password">New Password *</label>
                        <input type="password" name="password" id="password">
                    </div>


                    <div class="input-field col s6">
                        <label for="password_confirmation">Confirm Password *</label>
                        <input type="password" name="password_confirmation" id="password_confirmation">
                    </div>

                    <div class="col s6">
                        <button class="waves-effect waves-light btn cyan lighten-2" type="submit">Update</button>
                    </div>

                    <div class="col s6">
                        <p class="text-small">* Optional</p>
                    </div>
                </form>
            </div>
            <a href="#" id="openNotifications" class="waves-effect waves-light btn cyan lighten-2">Change notification settings</a>
        </div>
    </div>

    @include('partials/notifications')
@stop

@section('scripts')
    <script type="text/javascript" src="/js/account.js"></script>
@stop
