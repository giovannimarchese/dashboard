@extends('app')

@section('content')
    <div class="row home">

        <div class="col l8 m6 s12">
            {{--reputation--}}
            <div class="col l6 m12 s12">
                <a href="/reputation">
                    <div class="card-panel reputation hoverable cf">
                        <h4 class="reputation-head center">Server Reputation</h4>
                        <span class="low @if($reputation->min() <= $levels['red']) textRed @elseif($reputation->min() <= $levels['orange']) textOrange @else textGreen @endif">Low<br>{{$reputation->min()}}</span>
                        <span class="avg @if($reputation->avg() <= $levels['red']) textRed @elseif($reputation->avg() <= $levels['orange']) textOrange @else textGreen @endif">{{substr($reputation->avg(), 0, 2)}}</span>
                        <span class="high @if($reputation->max() <= $levels['red']) textRed @elseif($reputation->max() <= $levels['orange']) textOrange @else textGreen @endif">High<br>{{$reputation->max()}}</span>
                    </div>
                </a>
            </div>

            {{--volume--}}
            <div class="col l6 m12 s12">
                <a href="/volume">
                    <div class="card-panel volume hoverable">
                        <h4 class="volume-head center">Sent mails</h4>
                        <h5>Today: <b>{{$volume['today']}}</b></h5>
                        <h5>Yesterday: <b>{{$volume['yesterday']}}</b></h5>
                    </div>
                </a>
            </div>

            {{--mailing--}}
            <div class="col s12">
                <div class="card-panel hoverable">
                    <table class="highlight responsive-table bordered striped">
                        <thead>
                        <tr>
                            <th data-field="id">ID</th>
                            <th data-field="database">Customer</th>
                            <th data-field="name">Name</th>
                            <th data-field="mails">Mails</th>
                            <th data-field="timestamp">Timestamp</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mailings as $mailing)
                            <tr class="mailRow" data-mailing="MC{{$mailing->id}}">
                                <td>{{$mailing->old_mailing_id}}</td>
                                <td>{{$mailing->database->load('customer')->name}}</td>
                                <td class="tooltip" data-position="right" data-tooltip="{{$mailing->name}}">{{substr($mailing->name, 0, 40)}}</td>
                                <td>{{$mailing->num_mails}}</td>
                                <td>{{$mailing->starttime}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{--flag feed--}}
        <div class="col l4 m6 s12">
            <div class="col s12 flag-head">
                <a href="/flagged">
                    <div class="card-panel hoverable">
                        <h5 class="center">Flags</h5>
                    </div>
                </a>
            </div>
            <div class="col s12 flag-scroll home-feed">
                <div class="flagsWrapper">
                    @include('partials/flagsfeed')
                </div>
                <a class="loadMore btn-floating btn-large waves-effect waves-light cyan lighten-2" data-load="10"><i class="material-icons loadIcon">autorenew</i></a>
            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script src="/js/flag.js"></script>
    <script src="/js/home.js"></script>
@stop