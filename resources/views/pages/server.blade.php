@extends('app')

@section('content')
    <div class="row">
        <div class="col m8 offset-m2 s12">
            <div class="card-panel hoverable">
                <table class="responsive-table centered bordered striped">
                    <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="ip">IP</th>
                        <th data-field="name">Name</th>
                        <th data-field="locatio">Location</th>
                        <th data-field="type">Type</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($servers as $server)
                            <tr>
                                <td>{{$server->id}}</td>
                                <td>{{$server->ip}}</td>
                                <td>{{$server->name}}</td>
                                <td>{{$server->location}}</td>
                                <td>{{$server->type}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <button id="newServer" class="waves-effect waves-light btn cyan lighten-2" type="button"><i class="material-icons right">storage</i>new server</button>
        </div>
    </div>

    @include('partials/serverForm')
@stop

@section('scripts')
    <script type="text/javascript" src="/js/server.js"></script>
@stop