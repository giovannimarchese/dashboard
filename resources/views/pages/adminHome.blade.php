@extends('app')

@section('content')
<div class="row">
    <div class="col l8 offset-l2 m12 s12">
        <div class="col m6 s12">
            <div class="card-panel admin-panel hoverable">
                <h5>Add a new user</h5>
                <p>
                    Registering is done through admins, a user can't register himself.<br>
                    Once they've logged in, they can change the password you set for them.
                </p>
                <a href="/admin/register" class="waves-effect waves-light btn cyan lighten-2">New User</a>
            </div>
        </div>
        <div class="col m6 s12">
            <div class="card-panel admin-panel hoverable">
                <h5> Maintain all users</h5>
                <p>
                    Here you can edit & delete users.<br>
                    You can edit there user information, set a new password for a user or set/unset admin status.
                </p>
                <a href="/admin/users" class="waves-effect waves-light btn cyan lighten-2">Show Users</a>
            </div>
        </div>
        <div class="col m6 s12">
            <div class="card-panel admin-panel hoverable">
                <h5>Dashboard settings</h5>
                <p>
                    Here you can find all global settings for the application.<br>
                    You can set the variables that are used in various schedules or for automatic flags that are placed.
                </p>
                <a href="/admin/settings" class="waves-effect waves-light btn cyan lighten-2">Settings</a>
            </div>
        </div>
        <div class="col m6 s12">
            <div class="card-panel admin-panel hoverable">
                <h5>Add a new server</h5>
                <p>
                    If a new server has been added to the emark family you can add it to the dashboard here.
                </p>
                <a href="/admin/server" class="waves-effect waves-light btn cyan lighten-2">New server</a>
            </div>
        </div>
    </div>
</div>
    {!! csrf_field() !!}
@stop

@section('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/volume.js"></script>
@stop