@extends('app')

@section('content')

    <div class="row">
        <div class="col s12">
            <div class="card-panel hoverable cf">
                <p class="col m8 s12 repText">
                    This is an overview from all servers from the <b>last 10 days.</b><br>
                    You can change the amount of days you want to <b>search back</b> on the right.<br>
                    <b>To much lines?</b> Try clicking on a server in the legend.
                </p>
                <div class="col m4 s12">
                    <form class="inputDaysRight">
                        <input type="number" class="changeDaysInput" min="11" value="10">
                        <button type="submit" class="waves-effect cyan lighten-2 waves-light btn">Change days</button>
                    </form>
                </div>
                <div class="col s12">
                    <div id="linechart_material"></div>
                </div>
                <button href="#" class="flagIt waves-effect cyan lighten-2 waves-light btn right" flag-type="reputation">Flag reputation<i class="material-icons right">flag</i></button>
            </div>
        </div>
        <div class="col s12">
            <ul class="collapsible popout hoverable" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header hoverable active"><i class="material-icons">filter_drama</i>Senderscore</div>
                    <div class="collapsible-body hoverable">
                        <table class="responsive-table centered bordered striped senderscoreTable">
                            <thead>
                            <tr>
                                <th>IP Address</th>
                                <th>Servername</th>
                                <th>Location</th>
                                <th>Type</th>
                                <th>Score</th>
                                <th>Date</th>
                            </tr>
                            </thead>

                            <tbody class="senderscoreBody">
                                @foreach($senderscore as $score)
                                    @if(isset($score->scores[0]))
                                    <tr>
                                        <td>{{$score->ip}}</td>
                                        <td>{{$score->name}}</td>
                                        <td>{{$score->location}}</td>
                                        <td>{{$score->type}}</td>
                                        <td class="@if($score->scores[0]->score <= $levels['red']) textRed @elseif($score->scores[0]->score <= $levels['orange']) textOrange @else textGreen @endif">{{$score->scores[0]->score}}</td>
                                        <td>{{date('Y-m-d', strtotime($score->scores[0]->ts))}}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header hoverable"><i class="material-icons">filter_vintage</i>Senderbase</div>
                    <div class="collapsible-body hoverable">
                        <table class="responsive-table centered bordered striped senderbaseTable">
                            <thead>
                            <tr>
                                <th>IP Address</th>
                                <th>Servername</th>
                                <th>Reverse DNS</th>
                                <th>Reputation</th>
                                <th>Volume last day</th>
                                <th>Monthly volume</th>
                                <th>Volume change</th>
                                <th>Date</th>
                            </tr>
                            </thead>

                            <tbody class="senderbaseBody">
                            @foreach($senderbase as $base)
                                @if(isset($base->bases[0]))
                                <tr>
                                    <td>{{$base->ip}}</td>
                                    <td>{{$base->name}}</td>
                                    <td>{{$base->bases[0]->reverse_dns}}</td>
                                    <td>{{$base->bases[0]->email_reputation}}</td>
                                    <td>{{$base->bases[0]->magnitude_last_day}}</td>
                                    <td>{{$base->bases[0]->magnitude_last_month}}</td>
                                    <td>{{$base->bases[0]->volume_change}}</td>
                                    <td>{{date('Y-m-d', strtotime($base->bases[0]->ts))}}</td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header hoverable"><i class="material-icons">gradient</i>SNDS</div>
                    <div class="collapsible-body hoverable">
                        <table class="responsive-table centered bordered striped sndsTable">
                            <thead>
                            <tr>
                                <th>IP Address</th>
                                <th>Servername</th>
                                <th>From</th>
                                <th>Till</th>
                                <th>RCPT Cmds</th>
                                <th>DATA Cmds</th>
                                <th>Msg RCPT's</th>
                                <th>SPAM filter</th>
                                <th>SPAM traps</th>
                                <th>Complaint Rate</th>
                                <th>HELO</th>
                            </tr>
                            </thead>

                            <tbody class="sndsBody">
                            @foreach($snds as $sends)
                                @if(isset($sends->snds[0]))
                                <tr>
                                    <td>{{$sends->ip}}</td>
                                    <td>{{$sends->name}}</td>
                                    <td>{{date('Y-m-d', strtotime($sends->snds[0]->from))}}</td>
                                    <td>{{date('Y-m-d', strtotime($sends->snds[0]->till))}}</td>
                                    <td>{{$sends->snds[0]->rcpt}}</td>
                                    <td>{{$sends->snds[0]->data}}</td>
                                    <td>{{$sends->snds[0]->msg_rcpt}}</td>
                                    <td>{{$sends->snds[0]->filter}}</td>
                                    <td>{{$sends->snds[0]->trap}}</td>
                                    <td>{{$sends->snds[0]->complaint_rate}}</td>
                                    <td>{{$sends->snds[0]->helo}}</td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {!! csrf_field() !!}

    @include('partials/repForm')
@stop



@section('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/reputation.js"></script>
    <script type="text/javascript" src="/js/flag.js"></script>
@stop