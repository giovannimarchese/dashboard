@extends('app')

@section('content')

    <div class="row">
        <div class="col l7 m12">
            <div class="traffic-panel card-panel hoverable col s12">
                <p>This is an overview from mail traffic of <b class="selectedDate">selected date.</b></p>
                <div class="col s12">
                    <div class="col m6 s12">
                          <ul id="traffic" class="collection">
                            <li class="collection-item"><b>Traffic per server</b></li>
                          </ul>
                    </div>
                    <div class="col m6 s12">
                        <ul id="deliverability" class="collection">
                            <li class="collection-item"><b>Deliverability</b></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12">
                    <span class="inputDateRight">
                        <span class="tooltip" data-position="right" data-delay="100" data-tooltip="Sadly we don't have data of saturdays">?</span>
                        <input type="date" class="datepicker changeDateInput" value="{{\Carbon\Carbon::parse()->yesterday()->format('Y-m-d')}}">
                        <a class="waves-effect cyan lighten-2 waves-light btn changeDateBtn">Change date</a>
                        <a class="waves-effect cyan lighten-2 waves-light btn gotoMailingsBtn">Go to mailings</a>
                    </span>
                </div>
            </div>
            <div class="card-panel hoverable col s12">
                <p>
                    You can <b>click</b> on a legend item to <b>hide or show</b> it in the line chart.
                </p>
                <div id="linechart"></div>
            </div>
        </div>
        <div class="col l5 m12">
            <div class="card-panel hoverable">
                <div id="piechart1"></div>
                <div id="piechart2"></div>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}
@stop

@section('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/traffic.js"></script>
@stop