@extends('app')

@section('content')

    <div class="row">
        <div class="col s12">
            <div class="card-panel hoverable">
                <p><b>Pick two dates</b> to generate a volume charts of this period</p>
                <div class="inputDateRight">
                    <div class="input-field col s6">
                        <input type="date" id="date1" class="datepicker changeDate1Input" value="{{$weekAgo}}">
                        <label class="active" for="date1">From</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="date" id="date2" class="datepicker changeDate2Input" value="{{date('Y-m-d')}}">
                        <label class="active" for="date2">Until</label>
                    </div>
                    <a class="waves-effect cyan lighten-2 waves-light btn changeDateBtn">Generate</a>
                    </span>
                    <div id="linechart"></div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card-panel hoverable cf">
                <div class="col m6 s12">
                    This is an overview with <b>all time numbers!</b>
                    <ul class="collection">
                        <li class="collection-item">Total: <b>{{$volume['allTime']['total']}}</b></li>
                        <li class="collection-item">Delivered: <b>{{$volume['allTime']['delivered']}}</b> ({{$volume['allTime']['del_percentage']}})</li>
                        <li class="collection-item">Reads: <b>{{$volume['allTime']['reads']}}</b> ({{$volume['allTime']['read_percentage']}})</li>
                        <li class="collection-item">Clicks: <b>{{$volume['allTime']['clicks']}}</b> ({{$volume['allTime']['click_percentage']}})</li>
                        <li class="collection-item">Conversions: <b>{{$volume['allTime']['conversions']}}</b> ({{$volume['allTime']['conv_percentage']}})</li>
                        <li class="collection-item">Hardbounces: <b>{{$volume['allTime']['hardbounces']}}</b> ({{$volume['allTime']['hb_percentage']}})</li>
                        <li class="collection-item">Spamcomplaints: <b>{{$volume['allTime']['spam']}}</b> ({{$volume['allTime']['spam_percentage']}})</li>
                    </ul>
                </div>
                <div class="col m6 s12">
                    Overwiew of the <b>total amount of mails</b> that have been sent out during the year <b>{{date('Y')}}</b>.
                    <ul class="collection">
                        <li class="collection-item">This year: <b>{{$volume['thisYear']['total']}}</b> (delivered: {{$volume['thisYear']['delivered']}})</li>
                        <li class="collection-item">Last month: <b>{{$volume['lastMonth']['total']}}</b> (delivered: {{$volume['lastMonth']['delivered']}})</li>
                        <li class="collection-item">This month: <b>{{$volume['thisMonth']['total']}}</b> (delivered: {{$volume['thisMonth']['delivered']}})</li>
                        <li class="collection-item">Today: <b>{{$volume['today']['total']}}</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/css/sweetalert.css">
@stop

@section('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/lib/sweetalert.min.js"></script>
    <script type="text/javascript" src="/js/volumedates.js"></script>
@stop