@extends('app')

@section('content')
    <div class="row">
        <div class=" col l4 hide-on-med-and-down">
            <div class="card-panel hoverable">
                <p>
                    <b>Here you can find a list of all flags.</b>
                </p>
                <p>
                    Most flags are <b>placed by the dashboard,</b> but <b>users can also place flags</b> on the reputation & mailing page to mark that <b>something is going on with a server or a mailing.</b>
                <p>
                <p>
                    <b>Alert Levels:</b><br>
                    <span style="color: #f44336">&#9679;</span> - An urgent matter<br>
                    <span style="color: #ff9800">&#9679;</span> - This might be a problem<br>
                    <span style="color: #4caf50">&#9679;</span> - Something happened (not necessarily a problem)
                </p>
            </div>
        </div>
        <div class="col l4 m6 offset-m3 s12">
            <div class="col s12 flag-scroll">
                <div class="flagsWrapper">
                    @include('partials/flagsfeed')
                </div>
                <a class="loadMore btn-floating btn-large waves-effect waves-light cyan lighten-2" data-load="5"><i class="material-icons loadIcon">autorenew</i></a>
            </div>
            <div class="col s12">
                 <div class="card-panel hoverable note-panel">
                    @include('partials/noteForm')
                 </div>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}
@stop

@section('scripts')
    <script src="/js/flag.js"></script>
    <script src="/js/lib/jquery.validate.min.js"></script>
@stop