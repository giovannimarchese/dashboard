@extends('app')

@section('content')

    <div class="row">
        <div class="col l7 m12 s12">
            <div class="card-panel table-panel hoverable">
                <table id="overview_datatable" class="table table-hover table-mc-light-blue table-bordered table-striped responsive-table click-rows">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Databases</th>
                        <th>Mailings</th>
                        <th>Mails</th>
                        <th>Reads</th>
                        <th>Clicks</th>
                        <th>Conversions</th>
                        <th>HB</th>
                        <th>SB</th>
                        <th>SPAM</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                        @if($customer['mailings'] != 0)
                        <tr class="customer" data-customer="{{$customer['id']}}" data-cus-name="{{$customer['name']}}">
                            <td>{{$customer['id']}}</td>
                            <td>{{$customer['name']}}</td>
                            <td>{{$customer['db']}}</td>
                            <td>{{$customer['mailings']}}</td>
                            <td>{{$customer['mails']}}</td>
                            <td>{{$customer['reads']}}</td>
                            <td>{{$customer['clicks']}}</td>
                            <td>{{$customer['conversions']}}</td>
                            <td>{{$customer['hb']}}</td>
                            <td>{{$customer['sb']}}</td>
                            <td>{{$customer['spc']}}</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col l5 m12 s12">
            <div class="card-panel customer-panel hoverable">
                In this table we only show customers that have at least 1 active database!<br>
                <b>Click on a customer</b> to show mailing stats below.<br>
                Standard we show the <b>last 12 months</b>,<br>
                <b>Too expand this</b> use the input on the right
                <h5 class="chartheading"></h5>
                <div id="linechart"></div>
                <span class="inputMonthsRight hide">
                    <input type="number" class="changeMonthsInput" min="13" value="13">
                    <a class="waves-effect cyan lighten-2 waves-light btn changeMonthsBtn">Change Months</a>
                </span>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/css/sweetalert.css">
@stop

@section('scripts')
    <script type="text/javascript" src="/js/lib/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/lib/sweetalert.min.js"></script>
    <script type="text/javascript" src="/js/customer.js"></script>
@stop