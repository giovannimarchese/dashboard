@extends('app')

@section('content')

    <div class="row">
        <div class="col l10 s12 offset-l1">
            <div class="card-panel table-panel hoverable">
                <table id="overview_datatable" class="table table-hover table-mc-light-blue table-bordered table-striped responsive-table">
                    <thead>
                    <tr>
                        <th>Photo</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Admin</th>
                        <th>Created</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>@if($user->image != '')<img src="/img/profile/{{$user->image}}" class="imageTable">@endif</td>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->lastname}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->admin}}</td>
                            <td>{{substr($user->created_at, 0, 10)}}</td>
                            <td>@if($user->id != 2)<a href="/admin/edit/{{$user->id}}" class="editBtn">edit</a>@endif</td>
                            <td>@if($user->id != 2)<a href="/admin/delete/{{$user->id}}" class="delBtn">delete</a>@endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}

@stop

@section('styles')
    <link rel="stylesheet" href="/css/sweetalert.css">
@stop

@section('scripts')
    <script type="text/javascript" src="/js/lib/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/js/lib/sweetalert.min.js"></script>
    <script type="text/javascript" src="/js/admin.js"></script>
@stop