@extends('app')

@section('content')
    <input type="hidden" name="page" value="{{$page}}">
    @if(isset($customer_id))
        <input type="hidden" name="customer" value="{{$customer_id}}">
    @endif
    @if(isset($month))
        <input type="hidden" name="month" value="{{$month ? $month : ''}}">
    @endif
    @if(isset($date))
        <input type="hidden" name="date" value="{{$date ? $date : ''}}">
    @endif
    @if(isset($mailing))
        <input type="hidden" name="mailing" value="{{$mailing ? $mailing->name : ''}}">
        <input type="hidden" name="db-id" value="{{$mailing ? $mailing->customer_database_id : ''}}">
        <input type="hidden" name="db-name" value="{{$mailing ? $mailing->customer_db : ''}}">
        <input type="hidden" name="old-id" value="{{$mailing ? $mailing->old_mailing_id : ''}}">
    @endif
    <div class="row">
        <div class="col l7 m12 s12">
            <div class=" card-panel table-panel hoverable">
                <table id="overview_datatable" class="table table-hover table-mc-light-blue table-bordered table-striped responsive-table">
                    <thead>
                    <tr>
                        <th>Mail ID</th>
                        <th>Database</th>
                        <th>Name</th>
                        <th>Mails</th>
                        <th>Reads</th>
                        <th>Clicks</th>
                        <th>Conversions</th>
                        <th>HB</th>
                        <th>SB</th>
                        <th>SPAM</th>
                        <th>Timestamp</th>
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
        <div class="col l5 m12 s12">
            <div class="col s12 card-panel hoverable padding-panel">
                To see a simple <b>bar chart</b> of a mailing <b>click on a row</b> in the table
                <h5 class="mailname"></h5>
                <p class="customername"></p>
                <div id="barchart"></div>
                <a href="#" class="spam smallBtn dbBtn waves-effect cyan lighten-2 waves-light btn right hide" db-id="" mailing-id="">Spamcomplaints<i class="material-icons right">sms_failed</i></a>
                <a href="#" class="hardbounce smallBtn dbBtn waves-effect cyan lighten-2 waves-light btn right hide" db-id="" mailing-id="">Hardbounces<i class="material-icons right">call_missed</i></a>
                <a href="#" class="flagIt smallBtn waves-effect cyan lighten-2 waves-light btn right hide" flag-type="mailing" mailing-id="">Flag mailing<i class="material-icons right">flag</i></a>
            </div>

            <div class="col s12 card-panel hoverable padding-panel">
                <div class="col s12">
                    @if(isset($monthTitle))
                        <h5>Mailings from: {{$monthTitle}}</h5>
                    @else
                        The <b>table is loaded</b> with the mailings of <b>this month.</b><br>
                    @endif
                    If you want a mailings from a <b>different month,</b><br>
                    <b>Select a year & month here.</b>
                </div>
                <div class="col s6">
                    <input type="month" id="monthSelect" placeholder="yyyy-mm">
                </div>
                <div class="col s12">
                    <a id="loadMails" class="waves-effect cyan lighten-2 waves-light btn">Get Mails</a>
                </div>
            </div>

            <div class="col s12 card-panel hoverable padding-panel">
                @if(isset($customer))
                    <h4>Mailings from: {{$customer->name}}</h4>
                    @if($date != '')
                        <h4>On: {{$date}}</h4>
                    @endif
                @elseif(isset($date))
                    @if($date != '')
                        <h4>Mailings from: {{$date}}</h4>
                    @endif
                @endif
                <div class="col s12">
                    To get a more <b>specific set of mails,</b> <br>
                    you can <b>choose</b> a <b>Customer/Database</b> and/or a <b>Date</b>.<br>
                </div>
                <div class="col m4 s12">
                    <select id="customerSelect">
                        <option value="" selected>Customer</option>
                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col m4 s12 databases hide">
                    <select id="databaseSelect">
                        <option value="" selected>Database</option>
                    </select>
                </div>
                <div class="col m4 s12">
                    <input type="date" class="datepicker" id="dataSelect" placeholder="Pick a date here">
                </div>
                <div class="col s12">
                    <a id="getMails" class="waves-effect cyan lighten-2 waves-light btn">Get Mails</a>
                </div>
            </div>
        </div>
    </div>
    {!! csrf_field() !!}
    @include('partials/mailForm')
    @include('partials/spamcomplaints')
    @include('partials/hardbounces')
@stop


@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
@stop

@section('scripts')
    <script type="text/javascript" src="/js/lib/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/js/lib/select2.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/mailing.js"></script>
    <script type="text/javascript" src="/js/flag.js"></script>
@stop
