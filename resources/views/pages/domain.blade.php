@extends('app')

@section('content')
    <input type="hidden" name="date" value="{{$date}}">
    <div class="row">
        <div class="col l7 m12 s12">
            <div class="card-panel table-panel hoverable">
                <table id="overview_datatable" class="table table-hover table-mc-light-blue table-bordered table-striped responsive-table">
                    <thead>
                    <tr>
                        <th>Domain</th>
                        <th>Sent</th>
                        <th>Deferred</th>
                        <th>Percentage</th>
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
        <div class="col l5 m12 s12">
            <div class="col s12 domain card-panel hoverable padding-panel">
                The page is loaded with data from <b>{{$date}},</b><br>
                If you want data from a <b>different day</b><br>
                <b>Use the input below</b>
                <span class="inputDateRight">
                    <input type="date" class="changeDateInput datepicker" value="{{$date}}">
                    <a class="waves-effect cyan lighten-2 waves-light btn changeDateBtn">Change Date</a>
                </span>
            </div>
            <div class="col s12 card-panel hoverable padding-panel">
                <b>Click on a row</b> inside the table to display some <b>pie charts.</b>
                <h5 id="pieHeading"></h5>
                <div id="piechart1"></div>
                <div id="piechart2"></div>
                <div id="piechart3"></div>
            </div>
        </div>
    </div>


    {!! csrf_field() !!}
@stop

@section('scripts')
    <script type="text/javascript" src="/js/lib/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="/js/domain.js"></script>
@stop