<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif">

@foreach($flags as $flag)

    @if($flag['type'] == 'reputation')
        <a href="{{URL::to('/reputation')}}">
        @if($flag['foreign_id'] == 0)
            <span style="font-size: 20px"><b>Servers</b></span><br>
        @else
            <span style="font-size: 20px"><b>{{$flag['server']['name']}}</b></span><br>
        @endif
        </a>
    @elseif($flag['type'] == 'mailing')
        <a href="{{URL::to('/mailings/MC'.$flag['mailing']['id'])}}">
            <span style="font-size: 20px"><b>{{$flag['mailing']['name']}}</b></span><br>
        </a>
    @elseif($flag['type'] == 'note')
        <a href="{{URL::to('/flagged')}}">
            <span style="font-size: 20px"><b>Note</b></span><br>
        </a>
    @elseif($flag['type'] == 'update')
        <a href="{{URL::to('/flagged')}}">
            <span style="font-size: 20px"><b>Dashboard Update</b></span><br>
        </a>
    @endif
    <b>{{$flag['user']['name']}} {{$flag['user']['lastname']}}:</b> {{$flag['message']}}
    <br>
    <b>At:</b> {{$flag['updated_at']}}
    <br><br>
@endforeach

</body>
</html>