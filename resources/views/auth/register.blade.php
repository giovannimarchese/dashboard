@extends('app')

@section('content')

<div class="row">
    <div class="col l4 m6 s12 offset-l4 offset-m3">
        <div class="card-panel hoverable cf">

            <form method="POST" action="/admin/register">
                {!! csrf_field() !!}
                <h5 class="center">Register a user!</h5>
                <div class="input-field col s6">
                    <label for="firstname">Firstname</label>
                    <input type="text" name="name" value="{{ old('name') }}" id="firstname">
                </div>

                <div class="input-field col s6">
                    <label for="lastname">Lastname</label>
                    <input type="text" name="lastname" value="{{ old('lastname') }}" id="lastname">
                </div>

                <div class="input-field col s12">
                    <label for="email">Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" id="email">
                </div>

                <div class="input-field col s6">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password">
                </div>

                <div class="input-field col s6">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" name="password_confirmation" id="password_confirmation">
                </div>

                <div class="col s6">
                    <input type="checkbox" id="admin" name="admin" />
                    <label for="admin">Admin</label>
                </div>

                <div class="col s6">
                    <button class="waves-effect waves-light btn cyan lighten-2" type="submit">Register</button>
                </div>
            </form>

        </div>
    </div>
</div>

@stop