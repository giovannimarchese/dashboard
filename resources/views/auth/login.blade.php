@extends('app')

@section('content')


<div class="row">
    <div class="col s8 offset-s2 m6 offset-m3 l4 offset-l4">
        <div class="card-panel hoverable login-card">
            <form method="POST" action="/auth/login">
                {!! csrf_field() !!}

                <div class="input-field">
                    <i class="material-icons prefix">account_box</i>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" value="{{ old('email') }}">
                </div>

                <div class="input-field">
                    <i class="material-icons prefix">lock</i>
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password">
                </div>

                <p>
                    <input type="checkbox" name="remember" id="remember" />
                    <label for="remember">Remember Me</label>
                </p>

                <button class="btn cyan lighten-2 waves-effect waves-light login-btn" type="submit">Login
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
    </div>
</div>
@stop